<!-- plugins:js -->
<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('vendors/js/vendor.bundle.addons.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('js/off-canvas.js') }}"></script>
<script src="{{ asset('js/misc.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('js/dashboard.js') }}"></script>
<!-- End custom js for this page-->
<script src="{{ asset('js/chart.js') }}"></script>
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskMoney.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

<!-- Datatables -->
<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/jszip.min.js') }}"></script>



<script src="{{ asset('js/uploadFile.js') }}"></script>

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Selecione uma opção',
            language: {
                "noResults": function(){
                    return "Nenhum resultado encontrado";
                }
            },
        });

        $('.date').mask('00/00/0000', {placeholder: '__/__/____'});
        var SPMaskBehavior = function (val) {
         return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
        $('.phone_with_ddd').mask(SPMaskBehavior,spOptions);
        $('.cep').mask('00000-000', {placeholder: "_____-___"});
        $('.cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__"});
        $('.money').maskMoney();

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            todayBtn: "linked",
            todayHighlight: true
        });

    });
</script>

@stack('scripts')
