<?php

namespace App\Http\Controllers;

use App\CentroCusto;
use App\Carteira;
use App\Fornecedor;
use App\Receita;
use App\Subcategoria;
use App\Filters\ReceitaFilter;
use App\Http\Requests\ReceitaRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReceitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de receitas
     *
     * @return \Illuminate\Http\Response
     */
    public function list(ReceitaFilter $request)
    {
        $user = Auth::user();
        $receitas = Receita::filter($request)
            ->whereHas('empresa', function($q) use ($user) {
                $q->whereIn('empresa_id', $user->empresas->pluck('id'));
            })->paginate(10);

        return view('receitas.list')
            ->with('receitas', $receitas);
    }

    /**
     * Mostra o formulário de cadastro de receitas
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $user = Auth::user();
        $fornecedores = Fornecedor::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        $centros = CentroCusto::where('empresa_id', $user->empresas->pluck('id'))->get();
        $carteiras = Carteira::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');

        return view('receitas.add')
            ->with('fornecedores', $fornecedores)
            ->with('centros', $centros)
            ->with('carteiras', $carteiras);
    }

    /**
     * Grava receita no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ReceitaRequest $request)
    {
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;
        $data['data'] = Carbon::createFromFormat('d/m/Y', $data['data'])->format('Y-m-d');
        $data['valor'] = str_replace('.', '', $data['valor']);
        $data['valor'] = str_replace(',', '.', $data['valor']);

        Receita::create($data);

        return redirect()->route('receitas.list')
            ->with('success', 'Receita cadastrada com sucesso!');
    }

    /**
     * Mostra o formulário de editar receitas
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $receita = Receita::find($id);
        $fornecedores = Fornecedor::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        $centros = CentroCusto::where('empresa_id', $user->empresas->pluck('id'))->get();
        $carteiras = Carteira::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');


        return view('receitas.edit')
            ->with('receita', $receita)
            ->with('fornecedores', $fornecedores)
            ->with('centros', $centros)
            ->with('carteiras', $carteiras);
    }

    /**
     * Grava receitas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ReceitaRequest $request)
    {
        $data = $request->input();
        $receita = Receita::find($data['id']);
        $data['data'] = Carbon::createFromFormat('d/m/Y', $data['data'])->format('Y-m-d');
        $data['valor'] = str_replace('.', '', $data['valor']);
        $data['valor'] = str_replace(',', '.', $data['valor']);

        $receita->fill($data);
        $receita->save();

        return redirect()->route('receitas.list')
            ->with('success', 'Receita alterada com sucesso!');
    }

    /**
     * Excluir a receitas
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $receita = Receita::find($data['id']);

        if($receita->delete()) {

            return redirect()->route('receitas.list')
                ->with('success', 'Receita excluído com sucesso');

        }
            return redirect()->route('receitas.list')
                ->with('error', 'Não foi possível excluir essa receita. Por favor tente novamente.');

    }

}
