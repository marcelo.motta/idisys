<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class DespesaFilter
 *
 * @package App\Filters
 */
class DespesaFilter extends QueryFilters
{
	/**
	 * Do filter by name
	 *
	 * @param array $ids
	 *
	 * @return Builder
	 */
    public function s($s = '')
    {
		if($s != '')
			return $this->builder
                ->whereHas('fornecedor', function ($q) use ($s){
                    $q->where('nome', 'like', '%' . $s . '%');
                })
                ->orWhereHas('subcategoria', function ($q) use ($s){
                    $q->where('nome', 'like', '%' . $s . '%');
                });


	    return $this->builder;
	}
	
	public function status($status = '')
    {
		if($status != '')
			return $this->builder
                ->where('aprovada',$status);


	    return $this->builder;
	}

	


}
