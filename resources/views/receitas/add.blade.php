@extends('layouts.app')

@section('content')
    @role(['admin', 'manager']) 
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-success">CADASTRO DE RECEITAS</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('receitas.store') }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Fornecedor</label>
                                <div class="col-sm-9">
                                    {{ Form::select('fornecedor_id', $fornecedores, [], ['class' => 'form-control select2', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Centro de Custo</label>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="subcategoria_id" name="subcategoria_id">
                                        <option selected value=""></option>
                                        @foreach($centros as $centro)
                                            <optgroup label="{{$centro->nome}}">
                                                @foreach($centro->subcategorias as $subcategoria)                                            
                                                    <option value="{{$subcategoria->id}}">{{$subcategoria->nome}}</option>                                                                                                                                
                                                @endforeach
                                            </optgroup>                                                                                        
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Data emissão</label>
                                <div class="col-sm-9">
                                    <input class="form-control datepicker" name="data" value="{{ old('data') }}" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Valor R$</label>
                                <div class="col-sm-9">
                                    <input type="text" name="valor" value="{{ old('valor') }}" class="form-control money" required  data-affixes-stay="true" data-thousands="." data-decimal="," >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Carteira</label>
                                <div class="col-sm-9">
                                {{ Form::select('carteira_id', $carteiras, [], ['class' => 'form-control select2', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Observação</label>
                                <textarea rows="4" class="form-control" name="obs">{{ old('obs') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" class="btn" value="Limpar">
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endrole
        @role('operator')
        <h3>É necassario um login adminitrativo para acessar essas informacões</h3>
    @endrole
@endsection
