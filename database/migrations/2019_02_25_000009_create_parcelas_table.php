<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'parcelas';

    /**
     * Run the migrations.
     * @table parcelas
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('despesa_id')->unsigned();
            $table->date('data')->nullable();
            $table->double('valor')->nullable();
            $table->tinyInteger('situacao')->nullable();

            $table->timestamps();

            $table->index(["despesa_id"], 'fk_parcelas_despesas1_idx');


            $table->foreign('despesa_id', 'fk_parcelas_despesas1_idx')
                ->references('id')->on('despesas')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
