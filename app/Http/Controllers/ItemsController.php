<?php

namespace App\Http\Controllers;

use App\Item;
use App\Filters\ItemFilter;
use App\Http\Requests\ItemRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de items
     *
     * @return \Illuminate\Http\Response
     */
    public function list(ItemFilter $request)
    {
        $user = Auth::user();

        $items = Item::filter($request)
            ->whereHas('empresa', function($q) use ($user) {
                $q->whereIn('empresa_id', $user->empresas->pluck('id'));
            })->paginate(10);

        return view('items.list')
            ->with('items', $items);
    }

    /**
     * Mostra o formulário de items
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('items.add');
    }

    /**
     * Grava items no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;

        Item::create($data);

        return redirect()->route('items.list')
            ->with('success', 'Item cadastrado com sucesso!');
    }

    /**
     * Mostra o formulário de editar item
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $item = Item::find($id);

        return view('items.edit')
            ->with('item', $item);
    }

    /**
     * Grava item no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request)
    {
        $data = $request->input();
        $item = Item::find($data['id']);

        $item->fill($data);
        $item->save();

        return redirect()->route('items.list')
            ->with('success', 'Item alterado com sucesso!');
    }

    /**
     * Excluir item
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $item = Item::find($data['id']);

        if ($item->despesas->count()) {
            return redirect()->route('items.list')
                ->with('error', 'Esse item está vinculado a uma despesa.');
        }

        $item->delete();

        return redirect()->route('items.list')
            ->with('success', 'Item excluído com sucesso');

    }

}
