<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Filters\UserFilter;
use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the users list page
     *
     * @return \Illuminate\Http\Response
     */
    public function list(UserFilter $request)
    {
        $users = User::filter($request)->where('name', '!=', 'admin')->paginate(10);

        return view('users.list')
            ->with('users', $users);
    }

    /**
     * Mostra o formulário de cadastro de usuário
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $user_auth = Auth::user();
        if (!$user_auth->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }

        $empresas = Empresa::pluck('fantasia', 'id');
        $roles = Role::pluck('display_name', 'id');

        return view('users.add')
            ->with('empresas', $empresas)
            ->with('roles', $roles);
    }

    /**
     * Grava usuario no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $data = $request->input();

        $fileName = null;
        if ($request->hasFile('img')){
            $fileName = uniqid(date('HisYmd')) . '.' . $request->img->getClientOriginalExtension();
            $request->img->storeAs('users', $fileName);
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'photo' =>  $fileName
        ]);

        $user->attachRole($data['role_id']);
        $user->empresas()->sync($data['empresa_id']);

        return redirect()->route('users.list')
            ->with('success', 'Usuário cadastrado com sucesso!');
    }


    /**
     * Mostra o formulário de editar de usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_auth = Auth::user();
        if (!$user_auth->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }

        $user = User::find($id);

        $empresas = Empresa::pluck('fantasia', 'id');
        $roles = Role::pluck('display_name', 'id');

        return view('users.edit')
            ->with('user', $user)
            ->with('empresas', $empresas)
            ->with('roles', $roles);
    }


    /**
     * Grava usuario no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->input();
        $user = User::find($data['user_id']);
        $user->fill($data);

        $fileName = null;
        if ($request->hasFile('img')){

            // Salva a imagem nova
            $fileName = uniqid(date('HisYmd')) . '.' . $request->img->getClientOriginalExtension();
            $request->img->storeAs('users', $fileName);

            // Deleta a imagem antiga
            $user->delete_photo();

            // Atualiza no banco
            $user->fill([
                'photo' => $fileName
            ]);
        }

        $user->save();

        $user->roles()->sync($data['role_id']);
        $user->empresas()->sync($data['empresa_id']);

        return redirect()->route('users.list')
            ->with('success', 'Usuário atualizado com sucesso!');
    }

    /**
     * Mostra o formulário de editar senha de usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        $user = Auth::user();

        return view('users.update')
            ->with('user', $user);
    }


    /**
     * Grava usuario no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function passwordUpdate(UserPasswordRequest $request)
    {
        $data = $request->input();
        $user = Auth::user();
        $user->fill([
            'password' => Hash::make($data['password'])
        ]);

        $user->save();

        return redirect()->route('dashboard')
            ->with('success', 'Senha alterada com sucesso!');
    }

    /**
     * Excluir o usuário
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user_auth = Auth::user();
        if (!$user_auth->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }

        $data = $request->input();
        $user = User::find($data['id']);

        $user->delete();

        return redirect()->route('users.list')
            ->with('success', 'Usuário excluído com sucesso');
    }

    /**
     * Seta uma empresa para o usuário
     *
     * @return \Illuminate\Http\Response
     */
    public function setEmpresa($id){

        Auth::user()->setEmpresa($id);

        return redirect()->route('dashboard')
            ->with('success', 'Empresa alterada com sucesso');
    }
}
