<?php

namespace App\Http\Controllers;

use App\CentroCusto;
use App\Fornecedor;
use App\Filters\ParcelaFilter;
use App\Parcela;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contasPagar(ParcelaFilter $filter)
    {

        $user = Auth::user();

        $fornecedores = Fornecedor::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        $centros = CentroCusto::where('empresa_id', $user->empresas->pluck('id'))->get();
        $contas = Parcela::filter($filter)
            ->whereHas('despesa', function($d) use ($user){
                $d->whereHas('empresa', function($q) use ($user) {
                    $q->whereIn('empresa_id', $user->empresas->pluck('id'));
                })
                    ->where('aprovada', 1);


            })
            ->paginate(10);

        return view('reports.contas_pagar')
            ->with('filter', $filter->filters())
            ->with('contas', $contas)
            ->with('fornecedores', $fornecedores)
            ->with('centros', $centros);


    }
}
