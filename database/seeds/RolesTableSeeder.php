<?php

use App\Role;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Administrador'; // optional
        $admin->description  = 'Acesso geral'; // optional
        $admin->save();

        $leader = new Role();
        $leader->name         = 'manager';
        $leader->display_name = 'Gerente'; // optional
        $leader->description  = 'Responsável pela administração da empresa';
        $leader->save();

        $leader = new Role();
        $leader->name         = 'operator';
        $leader->display_name = 'Operador';
        $leader->description  = 'Responsável por realizar o lançamentos';
        $leader->save();

    }
}
