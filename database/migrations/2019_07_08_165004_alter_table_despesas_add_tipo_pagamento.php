<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDespesasAddTipoPagamento extends Migration
{

    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'despesas';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table($this->tableName, function (Blueprint $table) {

            $table->string('tipo_pagamento')->after('fornecedor_id')->default('conta');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn('tipo_pagamento');
        });
    }
}
