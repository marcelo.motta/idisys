<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'centro_custo_id'
    ];


    /**
     * Recebe o centro de custo
     */  
   
    public function centro_custos()
    {
        return $this->belongsTo('App\CentroCusto');
    }
    /**
     * Recebe as Receitas
     */
    public function receitas()
    {
        return $this->hasMany('App\Receita');
    }

    /**
     * Recebe as Despesas
     */
    public function despesas()
    {
        return $this->hasMany('App\Despesa');
    }

    /** Retorna o valor total de despesas
     * @return int
     */
    public function despesa_total($start_date = null, $end_date = null){
        $total = 0;

        $despesas = $this->despesas;
        if ($start_date and $end_date){
            $despesas = $despesas
                ->where('data', '>=', $start_date)
                ->where('data', '<=', $end_date);
        }

        foreach ($despesas as $despesa){
            $total = $total + $despesa->parcelas_total();
        }
        return $total;
    }

    /** Retorna o valor total de receitas
     * @return int
     */
    public function receita_total($start_date = null, $end_date = null){

        $receitas = $this->receitas;

        if($start_date and $end_date){
            $receitas = $receitas
                ->where('data', '>=', $start_date)
                ->where('data', '<=', $end_date);
        }
        $total = $receitas->sum('valor');


        return $total;
    }
}
