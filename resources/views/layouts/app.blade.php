<!DOCTYPE html>
{{--<html class="no-js" lang="{{ config('app.locale') }}">--}}
<head>
    @include('layouts.shared.head')
</head>
<body>

    @include('layouts.shared.nav')


    <div class="container-fluid page-body-wrapper">

        @include('layouts.shared.sidebar')

        <div class="main-panel">
            <div class="content-wrapper">
                @include('layouts.shared.flash')

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        @yield('content')

                    </div>
                </div>


            </div>
            @include('layouts.shared.footer')
        </div>

    </div>

    @include('layouts.shared.scripts')
</body>
</html>
