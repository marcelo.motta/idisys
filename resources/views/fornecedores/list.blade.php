@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-primary">FORNECEDORES</button>
                        <a href="{{ route('fornecedores.add') }}" class="btn btn-primary"><i class="mdi mdi-plus"></i> NOVO</a>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <form method="get" action="{{ route('fornecedores.list') }}">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Pesquisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <p class="card-description">

            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            CNPJ
                        </th>
                        <th>
                            CONTATO
                        </th>
                        <th>
                            TELEFONE
                        </th>
{{--                        @role(['admin', 'manager'])--}}
                        <th></th>
{{--                        @endrole--}}
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($fornecedores as $fornecedor)
                            <tr>
                            <td class="col-lg-10">
                                {{ $fornecedor->nome }}
                            </td>
                            <td class="col-lg-10">
                                {{ $fornecedor->cnpj }}
                            </td>
                            <td class="col-lg-10">
                                {{ $fornecedor->contato }}
                            </td>
                            <td class="col-lg-10">
                                {{ $fornecedor->telefone }}
                            </td>
                                <td class="text-right">
                                    <a href="{{ route('fornecedores.edit', $fornecedor->id) }}" class="btn btn-outline-primary btn-rounded"><i class="fa fa-edit"></i></a>
                                    @role(['admin', 'manager'])
                                    <a href="#" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $fornecedor->id }}" data-name="{{ $fornecedor->nome }}"  class="btn btn-outline-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                    @endrole
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>

            {{ $fornecedores->links() }}
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir empresa')

        <span id="message">Deseja excluir a empresa <strong></strong>?</span>

        <form method="post" action="{{ route('fornecedores.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $(this);
            modal.find('.modal-body #message strong').text(name);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            console.log($(this).parents().find('form'));
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
