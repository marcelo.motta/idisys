<?php

namespace App\Http\Controllers;

use App\CentroCusto;
use App\Filters\CentroCustoFilter;
use App\Http\Requests\CentroCustoRequest;
use App\Subcategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CentroCustosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Mostra a lista de centros
     *
     * @return \Illuminate\Http\Response
     */
    public function list(CentroCustoFilter $request)
    {

        $user = Auth::user();

        $centros = CentroCusto::filter($request)
            ->whereHas('empresa', function($q) use ($user) {
                $q->whereIn('empresa_id', $user->empresas->pluck('id'));
            })->paginate(10);

        return view('centros.list')
            ->with('centros', $centros);
    }

    /**
     * Mostra o formulário de cadastro de centros
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $empresas = Auth::user()->hasRole('admin');

        return view('centros.add')
            ->with('empresas', $empresas);
    }

    /**
     * Grava centros no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CentroCustoRequest $request)
    {
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;

        CentroCusto::create($data);

        return redirect()->route('centros.list')
            ->with('success', 'Centro de custo cadastrado com sucesso!');
    }

    /**
     * Mostra o formulário de editar centros
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centro = CentroCusto::find($id);

        return view('centros.edit')
            ->with('centro', $centro);
    }

    /**
     * Grava centros no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CentroCustoRequest $request)
    {
        $data = $request->input();
        $centro = CentroCusto::find($data['id']);

        $centro->fill($data);
        $centro->save();

        return redirect()->route('centros.list')
            ->with('success', 'CentroCusto alterado com sucesso!');
    }

    /**
     * Excluir a centro
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $centro = CentroCusto::find($data['id']);

        if ($centro->subcategorias->count() || $centro->subcategorias->count()) {
            return redirect()->route('centros.list')
                ->with('error', 'Esse centro de custos contém subcategoria(s) vinculada(s).');
        }

        $centro->delete();

        return redirect()->route('centros.list')
            ->with('success', 'Centro de custo excluído com sucesso');

    }

    /**
     * Mostra a lista de subcategorias
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_list($id)
    {

        $centro = CentroCusto::find($id);

        return view('centros.subcategorias.list')
            ->with('centro', $centro);
        

    }

    /**
     * Mostra o formulário de cadastro de subcategoria
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_add($id)
    {
        $centro = CentroCusto::find($id);
        return view('centros.subcategorias.add')
            ->with('id', $id);
    }

    /**
     * Grava subcategoria no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_store(CentroCustoRequest $request)
    {
        $data = $request->input();
        Subcategoria::create($data);      
        return redirect()->route('subcategorias.list',$data["centro_custo_id"])        
        ->with('success', 'Subcategoria cadastrado com sucesso!');
    }

    /**
     * Mostra o formulário de editar subcategoria
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_edit($id)
    {
        $subcategoria = Subcategoria::find($id);

        return view('centros.subcategorias.edit')
            ->with('subcategoria', $subcategoria);
    }

    /**
     * Grava subcategoria no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_update(CentroCustoRequest $request)
    {
        $data = $request->input();
        $subcategoria = Subcategoria::find($data['id']);

        $subcategoria->fill($data);
        $subcategoria->save();

        return redirect()->route('subcategorias.list',$data["centro_custo_id"])
            ->with('success', 'Subcategoria alterado com sucesso!');
    }

    /**
     * Excluir a subcategoria
     *
     * @return \Illuminate\Http\Response
     */
    public function sub_delete(Request $request)
    {   
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $subcategoria = Subcategoria::find($data['id']);

        if ($subcategoria->despesas->count() || $subcategoria->receitas->count()) {
            return redirect()->route('subcategorias.list',$data["centro"])
                ->with('error', 'Essa subcategoria está vinculado a uma receita ou despesa.');
        }
       
        $subcategoria->delete();

        return redirect()->route('subcategorias.list',$data["centro"])
            ->with('success', 'subcategoria excluído com sucesso');

    }

}
