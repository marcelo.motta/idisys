<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Despesa extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empresa_id',
        'subcategoria_id',
        'carteira_id',
        'fornecedor_id',
        'numero',
        'tipo_pagamento',
        'data',
        'aprovada',
        'obs'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Recebe a Empresa
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    /**
     * Recebe a subcategoria do Centro de custo
     */
    public function subcategoria()
    {
        return $this->belongsTo('App\Subcategoria');
    }

    /**
     * Recebe a Carteira
     */
    public function carteira()
    {
        return $this->belongsTo('App\Carteira');
    }

    /**
     * Recebe o Fornecedor
     */
    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedor');
    }

    /**
     * Recebe as Parcelas
     */
    public function parcelas()
    {
        return $this->hasMany('App\Parcela');
    }

    /**
     * Recebe os Itens
     */
    public function itens()
    {
        return $this->belongsToMany('App\Item')->withPivot(['valor', 'atencao']);
    }


    /**
     * Calcula o valor total da despesa
     */
    public function valor(){

        $valor = 0;

        foreach ($this->itens as $item){
            $valor = $valor + $item->pivot->valor;
        }
        return $valor;
    }

    /**
     * Calcula o valor total das parcelas da despesa
     */
    public function parcelas_total(){

        return $this->parcelas->sum('valor');
    }

    /**
     * Calcula diferença do valor das parcelas com total da despesa
     */
    public function despesa_parcelas_diferenca(){

        return number_format($this->valor() - $this->parcelas_total(), 2);
    }

}
