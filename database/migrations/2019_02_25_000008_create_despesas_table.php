<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'despesas';

    /**
     * Run the migrations.
     * @table despesas
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('centro_custo_id')->unsigned();
            $table->integer('fornecedor_id')->unsigned();
            $table->date('data')->nullable();
            $table->tinyInteger('aprovada')->nullable();
            $table->string('obs')->nullable();

            $table->timestamps();

            $table->index(["empresa_id"], 'fk_despesas_empresas1_idx');

            $table->index(["fornecedor_id"], 'fk_despesas_fornecedores1_idx');

            $table->index(["centro_custo_id"], 'fk_despesas_centro_custos1_idx');


            $table->foreign('centro_custo_id', 'fk_despesas_centro_custos1_idx')
                ->references('id')->on('centro_custos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('empresa_id', 'fk_despesas_empresas1_idx')
                ->references('id')->on('empresas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('fornecedor_id', 'fk_despesas_fornecedores1_idx')
                ->references('id')->on('fornecedores')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
