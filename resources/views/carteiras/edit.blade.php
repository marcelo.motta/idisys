@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">EDITAR CARTEIRA</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('carteiras.update') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $carteira->id }}">
                    <p class="card-description">
                        <b>DADOS DE CARTEIRA</b>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME</label>
                                <div class="col-sm-10">
                                    <input name="nome" type="text" value="{{ $carteira->nome }}" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" required>
                                    <div class="invalid-feedback">
                                        Nome é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('carteiras.edit', $carteira->id) }}" class="btn btn-light btn-fw">Cancelar</a>
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
