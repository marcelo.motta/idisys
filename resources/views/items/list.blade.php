@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-primary">ITENS</button>
                        <a href="{{ route('items.add') }}" class="btn btn-primary"><i class="mdi mdi-plus"></i> NOVO</a>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <form method="get" action="{{ route('items.list') }}">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Pesquisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <p class="card-description">

            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        @role(['admin', 'manager'])
                        <th></th>
                        @endrole
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr>
                            <td class="col-lg-10">
                                {{ $item->nome }}
                            </td>
                            @role(['admin', 'manager'])
                                <td class="text-right">
                                    <a href="{{ route('items.edit', $item->id) }}" class="btn btn-outline-primary btn-rounded"><i class="fa fa-edit"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $item->id }}" data-name="{{ $item->nome }}"  class="btn btn-outline-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                </td>
                            @endrole
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>

            {{ $items->links() }}
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir Item')

        <span id="message">Deseja excluir a empresa <strong></strong>?</span>

        <form method="post" action="{{ route('items.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $(this);
            modal.find('.modal-body #message strong').text(name);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            console.log($(this).parents().find('form'));
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
