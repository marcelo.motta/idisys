<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class FornecedorFilter
 *
 * @package App\Filters
 */
class ItemFilter extends QueryFilters
{
	/**
	 * Do filter by name
	 *
	 * @param array $ids
	 *
	 * @return Builder
	 */
    public function name($name = '')
    {
		if($name != '')
			return $this->builder
                ->where('nome', 'like', '%' . $name . '%');


	    return $this->builder;
	}


}
