<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class DespesaItem extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idem_id',
        'despesa_id',
        'valor',
        'atencao'
    ];

    /**
     * Define manualmente a tabela
     */
    protected $table = 'despesa_item';

    /**
     * Recebe a Empresa do Fornecedor
     */
    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    /**
     * Recebe as despesas do Fornecedor
     */
    public function despesa()
    {
        return $this->belongsTo('App\Despesa');
    }

}
