<?php

namespace App\Filters;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FornecedorFilter
 *
 * @package App\Filters
 */
class ParcelaFilter extends QueryFilters
{
    /**
     * Do filter by data
     *
     * @param array $ids
     *
     * @return Builder
     */
    public function data($date = '')
    {
        if($date != '')
            return $this->builder
                ->where('data', '>=', $date . ' 00:00:00')
                ->where('data', '<=', $date . ' 23:59:59');


        return $this->builder;
    }

    /**
     * Do filter by mes
     *
     * @param array $ids
     *
     * @return Builder
     */
    public function mes($mes = '')
    {
        if($mes != '')
            $mes = \Carbon\Carbon::createFromFormat('m-Y', $mes);
            return $this->builder
                ->where('data', '>=', $mes->startOfMonth()->format('Y-m-d'))
                ->where('data', '<=', $mes->endOfMonth()->format('Y-m-d'));


        return $this->builder;
    }


    /**
     * @param null $period
     * @return Builder
     */
    public function startDate($date = null)
    {

        if($date){

            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');

            return $this->builder->where('data', '>=', $date);
        }

    }

    /**
     * @param null $period
     * @return Builder
     */
    public function finalDate($date = null)
    {

        if($date){

            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');

            return $this->builder->where('data', '<=', $date);
        }

    }
    /**
     * Do filter by fornecedor
     *
     * @param array $ids
     *
     * @return Builder
     */
    public function fornecedor($fornecedor ='' )
    {
              
        if($fornecedor != '')        
            $this->builder
                ->whereHas('despesa', function ($q) use ($fornecedor){
                   $q->where('fornecedor_id',$fornecedor);
               });                      

    }
    public function centro($centro ='')
    {

        if($centro != '')        
            $this->builder
                ->whereHas('despesa', function ($q) use ($centro){
                   $q->where('subcategoria_id',$centro);
               });

    }
    
}
