<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReceitaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subcategoria_id' => 'required',
            'fornecedor_id' => 'required',
            'carteira_id' => 'required',
            'data' => 'required',
            'valor' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'empresa_id.required' => 'O campo :attribute é obrigatório',
            'subcategoria_id.required' => 'O campo :attribute é obrigatório',
            'carteira_id.required' => 'O campo :attribute é obrigatório',
            'fornecedor_id.required' => 'O campo :attribute é obrigatório',
            'data.required' => 'O campo :attribute é obrigatório',
            'valor.required' => 'O campo :attribute é obrigatório',
        ];
    }
}
