@if(Session::has('success'))
    <div class="text-success">
        {{ Session::get('success') }}
    </div>
@endif
@if(Session::has('error'))
    <div class="text-danger" role="alert">
        {{ Session::get('error') }}
    </div>
@endif
@if(Session::has('info'))
    <div class="text-info" role="alert">
        {{ Session::get('info') }}
    </div>
@endif
@if($errors->any())
    @foreach ($errors->all() as $error)
        <div class="text-danger" role="alert">
            <strong>Erro!</strong> {{$error}}
        </div>
    @endforeach
@endif
