@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">EDITAR DE FORNECEDOR</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('fornecedores.update') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $fornecedor->id }}">
                    <p class="card-description">
                        <b>DADOS DO FORNECEDOR</b>
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME</label>
                                <div class="col-sm-10">
                                    <input name="nome" type="text" value="{{ $fornecedor->nome }}" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Nome é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">CONTATO</label>
                                <div class="col-sm-10">
                                    <input name="contato" type="text" value="{{ $fornecedor->contato }}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">CNPJ</label>
                                <div class="col-sm-10">
                                    <input name="cnpj" type="text" value="{{ $fornecedor->cnpj }}" class="form-control cnpj {{ $errors->has('cnpj') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        CNPJ é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">TELEFONE</label>
                                <div class="col-sm-10">
                                    <input name="telefone" type="text" value="{{ $fornecedor->telefone }}" class="form-control phone_with_ddd" placeholder="(__) ____-____" maxlength="14">
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="card-description">
                        <b> DADOS BANCÁRIOS</b>
                    </p>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">BANCO</label>
                                <div class="col-sm-9">
                                    <input name="banco" type="text" class="form-control" value="{{ $fornecedor->banco }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">AGÊNCIA</label>
                                <div class="col-sm-9">
                                    <input name="ag" type="text" class="form-control" value="{{ $fornecedor->ag }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">CONTA CORRENTE</label>
                                <div class="col-sm-9">
                                    <input name="conta" type="text" class="form-control" value="{{ $fornecedor->conta }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">POUPANÇA</label>
                                <div class="col-sm-9">
                                    <input name="poupanca" type="text" class="form-control" value="{{ $fornecedor->poupanca }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">DADOS NOMINAIS</label>
                                <div class="col-sm-9">
                                    <input name="dados_nominais" type="text" class="form-control" value="{{ $fornecedor->dados_nominais }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('fornecedores.edit', $fornecedor->id) }}" class="btn btn-light btn-fw">Cancelar</a>
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
