<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesaItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despesa_item', function (Blueprint $table) {

            $table->integer('item_id')->unsigned();
            $table->integer('despesa_id')->unsigned();
            $table->double('valor')->nullable();
            $table->tinyInteger('atencao')->nullable();

            $table->index(["despesa_id"], 'fk_despesa_item_item_idx');
            $table->index(["item_id"], 'fk_despesa_item_despesa_idx');


            $table->foreign('item_id', 'fk_despesa_item_despesa_idx')
                ->references('id')->on('itens')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('despesa_id', 'fk_despesa_item_item_idx')
                ->references('id')->on('despesas')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despesa_item');
    }
}
