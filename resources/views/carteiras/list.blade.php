@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-primary">CARTEIRAS</button>
                        <a href="{{ route('carteiras.add') }}" class="btn btn-primary"><i class="mdi mdi-plus"></i> NOVO</a>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <form method="get" action="{{ route('carteiras.list') }}">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Pesquisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <p class="card-description">

            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        @role(['admin', 'manager'])
                        <th></th>
                        @endrole
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($carteiras as $carteira)
                            <tr>
                            <td class="col-lg-10">
                                {{ $carteira->nome }}                             
                            </td>
                            @role(['admin', 'manager'])
                                <td class="text-right">                                    
                                    <a href="{{ route('carteiras.edit', $carteira->id) }}" class="btn btn-outline-warning btn-rounded"><i class="fa fa-edit"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $carteira->id }}" data-name="{{ $carteira->nome }}"  class="btn btn-outline-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                </td>
                            @endrole
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>
         
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir empresa')

        <span id="message">Deseja excluir a empresa <strong></strong>?</span>

        <form method="post" action="{{ route('carteiras.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $(this);
            modal.find('.modal-body #message strong').text(name);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            console.log($(this).parents().find('form'));
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
