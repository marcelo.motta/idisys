@extends('layouts.auth')

@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                        <div class="auto-form-wrapper">
                            @include('layouts.shared.flash')

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Recuperação de senha</h4>
                                <span class="text-info text-small">Entre com seu email para recupação da senha.</span>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="email" class="col-md-3 col-form-label">Email</label>

                            <div class="col-md-9">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if (session('status'))
                            <p class="text-success">{{ session('status') }}</p>
                        @endif

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-6 text-right">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <p class="clearfix"></p>
                <p class="footer-text text-center">Copyright © 2019 Idisys by <a href="http://mxserv.com.br" target="_new">mxserv</a>. Todos os direitos reservados.</p>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
@endsection
