<div class="modal fade" id="{{ $modal }}" tabindex="-1" role="dialog" aria-labelledby="{{ $modal }}Label" aria-hidden="true">
    <div class="modal-dialog {{ isset($size) ? $size : null }}" role="document">
        <div class="modal-content {{ isset($color) ? $color : null }}">
            <div class="modal-header">
                <h4 class="modal-title">{{ isset($title) ? $title : null }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {{ $slot }}

            </div>
            @if(isset($footer))
            <div class="modal-footer">
                {{ $footer }}
            </div>
            @endif

        </div>
    </div>
</div>
