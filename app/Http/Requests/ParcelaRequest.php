<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParcelaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'despesa_id' => 'required',
            'data' => 'required',
            'valor' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'despesa_id.required' => 'O campo despesa é obrigatório',
            'data.required' => 'O campo data é obrigatório',
            'valor.required' => 'O valor nome é obrigatório',
        ];
    }
}
