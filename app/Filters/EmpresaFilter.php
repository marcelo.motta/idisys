<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class EmpresaFilter
 *
 * @package App\Filters
 */
class EmpresaFilter extends QueryFilters
{
	/**
	 * Do filter by name
	 *
	 * @param array $ids
	 *
	 * @return Builder
	 */
    public function name($name = '')
    {
		if($name != '')
			return $this->builder
                ->where('razaosocial', 'like', '%' . $name . '%')
				->orWhere('fantasia', 'like', '%' . $name . '%');


	    return $this->builder;
	}


}
