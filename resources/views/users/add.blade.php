@extends('layouts.app')

@section('content')

    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">CADASTRO DE USUÁRIO</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('users.store') }}" enctype="multipart/form-data">
                    @csrf
                    <p class="card-description">
                        <b>DADOS DO USUÁRIO</b>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME</label>
                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}">
                                    <div class="invalid-feedback">
                                        Nome é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input name="email" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Email é obrigatório e precisa ser único e válido
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">SENHA</label>
                                <div class="col-sm-10">
                                    <input name="password" type="password" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Senha é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">FOTO</label>
                                <div class="col-sm-10">
                                    <input type="file" name="img" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" placeholder="Selecione uma foto">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info" type="button">Selecionar</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <p class="card-description">
                    <b>DADOS DE ACESSO</b>
                    </p>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">EMPRESA</label>
                                <div class="col-sm-10">
                                    <select name="empresa_id" id="empresa" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                        <option></option>
                                        @foreach($empresas as $key => $empresa)
                                            <option value="{{ $key }}">{{ $empresa }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Empresa é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">PERMISSÃO</label>
                                <div class="col-sm-10">
                                    <select name="role_id" id="role" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                        <option></option>
                                        @foreach($roles as $key => $role)
                                            <option value="{{ $key }}">{{ $role }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Permissão é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" class="btn" value="Limpar">
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
