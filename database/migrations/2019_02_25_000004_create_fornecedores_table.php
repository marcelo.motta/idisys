<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'fornecedores';

    /**
     * Run the migrations.
     * @table fornecedores
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->string('nome', 80)->nullable();
            $table->string('cnpj', 45)->nullable();
            $table->string('banco', 45)->nullable();
            $table->string('ag', 45)->nullable();
            $table->string('conta', 45)->nullable();
            $table->string('poupanca', 45)->nullable();
            $table->string('dados_nominais', 45)->nullable();

            $table->timestamps();

            $table->index(["empresa_id"], 'fk_fornecedores_empresas1_idx');


            $table->foreign('empresa_id', 'fk_fornecedores_empresas1_idx')
                ->references('id')->on('empresas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
