@extends('layouts.dashboard')

@section('content')


    @role(['admin', 'manager'])
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi mdi-bank text-facebook icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">SALDO ATUAL</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{ money($empresa_atual->saldo_atual(), true) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi  mdi-trending-up text-success icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">RECEITAS</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{ money($empresa_atual->receita_total(\Carbon\Carbon::now()->format('m-Y')), true) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi mdi-trending-down text-danger icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">DESPESAS</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{ money($empresa_atual->despesa_total(\Carbon\Carbon::now()->format('m-Y')), true) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi mdi-checkbox-multiple-blank text-info icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">LANCAMENTOS</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{ $empresa_atual->total_lancamentos(\Carbon\Carbon::now()->format('m-Y')) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole
    <div class="row">  
        @role(['admin', 'manager'])     
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                    <h4 class="card-title">Receitas do mês</h4>
                    @component('shared._charts')

                        @slot('chart', 'receitas_mes')
                        @slot('data', $receitas_centro_chart)


                    @endcomponent
                </div>
            </div>
        </div>
        @endrole
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0">

                            </div>
                        </div>
                    </div>
                    <h4 class="card-title">Despesas do mês</h4>                    
                    @component('shared._charts')

                        @slot('chart', 'despesas_mes')
                        @slot('data', $despesas_centro_chart )


                    @endcomponent
                </div>
            </div>
        </div>
        @role('operator')

        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">  
                    <h4 class="card-title">Contas a pagar</h4>                  
                    <p class="card-description">
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                            <thead>
                            <tr>
                                <th>
                                    DOC
                                </th>
                                <th>
                                    VENCIMENTO
                                </th>
                                <th>
                                    VALOR
                                </th>
                                <th>
                                    FORNECEDOR
                                </th>
                               
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($table as $parcela)
                                    <tr>
                                        <td >{{ $parcela->numero }}</td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($parcela->data)->format('d/m/Y') }}
                                        </td>
                                        <td>
                                            R$ {{ number_format($parcela->valor, 2, ',', '.') }}
                                        </td>
                                        <td >{{ $parcela->despesa->fornecedor->nome }}</td>
                                                                         
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <p class="clearfix"></p>

                   <a href="{{route('contasapagar.list')}}">Ver todas as contas</a>
                   
                </div>
            </div>
        </div>
        @endrole
        @role(['admin', 'manager']) 
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">                            
                            <h4 class="card-title"> Despesas x Receitas por centro de custo</h4>                            
                        </div>
                        <div class="col-md-6 text-right">
                            <form id="setCentroFrom" action="{{ route('dashboard') }}" method="post">
                                @csrf
                                {{ Form::select('centro_id', $centros_custo, $centro_custo_selected, ['class' => 'form-control select2', 'required' => 'required', 'placeholder' => 'Selecione uma opção']) }}
                            </form>
                        </div>
                    </div>
                    
                    @component('shared._charts')
                    

                        @slot('chart', 'despesas_receitas_centro')
                        @slot('data', $despesas_receitas_centro_chart )


                    @endcomponent
                </div>
            </div>
        </div>

        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title">Despesas x Receitas por carteira</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <form id="setCarteiraFrom" action="{{ route('dashboard') }}" method="post">
                                @csrf
                                {{ Form::select('carteira_id', $carteira, $carteira_selected, ['class' => 'form-control select2', 'required' => 'required', 'placeholder' => 'Selecione uma opção']) }}
                            </form>
                        </div>
                    </div>

                    @component('shared._charts')

                        @slot('chart', 'despesas_receitas_carteira')
                        @slot('data', $despesas_receitas_carteira_chart )


                    @endcomponent
                </div>
            </div>
        </div>
        @endrole
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Itens com atenção</h4>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Centro de custo</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$despesas_atencao->isEmpty())
                                @foreach($despesas_atencao as $atencao)
                                    <tr>
                                        <td>{{ $atencao->item->nome }}</td>
                                        <td>{{ $atencao->despesa->subcategoria->nome }}</td>
                                        <td>{{ money($atencao->valor, true) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="alert-warning text-center">Não existem itens em atenção para esse período</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>
        $('#setCentroFrom select').on('change', function () {
            $(this).parent().submit();
        });
    </script>

    <script>
        $('#setCarteiraFrom select').on('change', function () {
            $(this).parent().submit();
        });
    </script>

@endpush
