<?php


function money($valor, $sigla = false){
    echo $sigla ? 'R$ ' : '';
    echo number_format($valor, 2, ',', '.');
}

function dynamicColors(){
    $rgbColor = array();

    foreach(array('r', 'g', 'b') as $color){
        //Generate a random number between 0 and 255.
        $rgbColor[$color] = mt_rand(0, 255);
    }

    return $rgbColor;
}
?>
