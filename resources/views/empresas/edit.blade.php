@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">EDITAR EMPRESA</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('empresas.update') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $empresa->id }}">
                    <p class="card-description">
                        <b>DADOS DA EMPRESA</b>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">RAZÃO SOCIAL</label>
                                <div class="col-sm-10">
                                    <input name="razaosocial" type="text" value="{{ $empresa->razaosocial }}" class="form-control {{ $errors->has('razaosocial') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Razão social é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME FANTASIA</label>
                                <div class="col-sm-10">
                                    <input name="fantasia" type="text" value="{{ $empresa->fantasia }}" class="form-control {{ $errors->has('fantasia') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Nome fantasia é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">CNPJ</label>
                                <div class="col-sm-10">
                                    <input name="cnpj" type="text" value="{{ $empresa->cnpj }}" class="form-control cnpj {{ $errors->has('cnpj') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        CNPJ é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <p class="card-description">
                    <b> ENDEREÇO</b>
                    </p>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">ENDEREÇO</label>
                                <div class="col-sm-9">
                                    <input name="endereco" value="{{ $empresa->endereco }}" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">CEP</label>
                                <div class="col-sm-9">
                                    <input name="cep" value="{{ $empresa->cep }}" type="text" class="form-control cep">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">TELEFONE</label>
                                <div class="col-sm-9">
                                    <input name="telefone" value="{{ $empresa->telefone }}" type="text" class="form-control phone_with_ddd">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">RESPONSÁVEL</label>
                                <div class="col-sm-9">
                                    <input name="responsavel" value="{{ $empresa->responsavel }}" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('empresas.edit', $empresa->id) }}" class="btn btn-light btn-fw">Cancelar</a>
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
