@extends('layouts.auth')

@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <div class="auto-form-wrapper">
                            <div class="row">
                                <div class="col -lg-12 text-center">
                                    <img src="{{ asset('images/idisys.png') }}" alt="">
                                </div>
                            </div>
                            <p class="clearfix"></p>
                            <form role="form" method="POST" action="{{ route('login') }}" data-toggle="validator">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="label">Email</label>
                                    <div class="input-group">
                                        <input name="email" type="text" class="form-control" placeholder="Email">
                                        <div class="input-group-append">
                                          <span class="input-group-text">
                                            <i class="mdi mdi-email"></i>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label">Senha</label>
                                    <div class="input-group">
                                        <input name="password" type="password" class="form-control" placeholder="*********">
                                        <div class="input-group-append">
                                          <span class="input-group-text">
                                            <i class="mdi mdi-account-key"></i>
                                          </span>
                                        </div>
                                    </div>
                                </div>

                                <p>@include('layouts.shared.login-messages')</p>
                                <div class="form-group">
                                    <button class="btn btn-primary submit-btn btn-block">Login</button>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <div class="form-check form-check-flat mt-0">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" checked> Manter logado
                                        </label>
                                    </div>
                                    <a href="{{ route('password.request') }}" class="text-small forgot-password text-black">Esqueceu a senha?</a>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<button class="btn btn-block g-login">--}}
                                        {{--<img class="mr-3" src="../../images/file-icons/icon-google.svg" alt="">Log in with Google</button>--}}
                                {{--</div>--}}
                                {{--<div class="text-block text-center my-3">--}}
                                    {{--<span class="text-small font-weight-semibold">Not a member ?</span>--}}
                                    {{--<a href="register.html" class="text-black text-small">Create new account</a>--}}
                                {{--</div>--}}
                            </form>
                        </div>
                        {{--<ul class="auth-footer">--}}
                            {{--<li>--}}
                                {{--<a href="#">Conditions</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">Help</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">Terms</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        <p class="clearfix"></p>
                        <p class="footer-text text-center">Copyright © 2019 Idisys by <a href="http://mxserv.com.br" target="_new">mxserv</a>. Todos os direitos reservados.</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

@endsection
