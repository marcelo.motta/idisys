<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Parcela extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'despesa_id',
        'data',
        'valor',
        'situacao',
        'numero'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Recebe a Despesa
     */
    public function despesa()
    {
        return $this->belongsTo('App\Despesa');
    }


}
