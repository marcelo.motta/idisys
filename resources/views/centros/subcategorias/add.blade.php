@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">CADASTRO SUBCATEGORIA</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('subcategorias.store') }}">
                    @csrf
                    <p class="card-description">
                        <b>DADOS DA SUBCATEGORIA</b>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME</label>
                                <div class="col-sm-10">
                                    <input name="nome" type="text" value="{{ old('nome') }}" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}">
                                    <div class="invalid-feedback">
                                        Nome é obrigatório
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" class="btn" value="Limpar">
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                    <input type="hidden" name="centro_custo_id" value="{{ $id }}">
                </form>
            </div>
        </div>
@endsection
