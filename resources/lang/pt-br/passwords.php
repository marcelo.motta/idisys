<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha precisa ter pelo menos 6 caracteres e ser igual a confirmação',
    'reset' => 'Sua senha foi restaurada!',
    'sent' => 'Foi enviado um link de recuperação para seu email',
    'token' => 'Token inválido',
    'user' => "Não localizamos seu endereço de email.",

];
