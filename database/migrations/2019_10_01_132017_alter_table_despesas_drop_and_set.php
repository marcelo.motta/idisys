<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDespesasDropAndSet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('despesas', function (Blueprint $table) {                       
            $table->integer('carteira_id')->unsigned();            
            $table->foreign('carteira_id')->references('id')->on('carteiras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('despesas', function (Blueprint $table) {
            $table->dropForeign('despesas_carteira_id_foreign');            
            $table->dropColumn('carteira_id'); 
        });
    }
}
