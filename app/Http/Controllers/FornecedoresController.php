<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Fornecedor;
use App\Filters\FornecedorFilter;
use App\Http\Requests\FornecedorRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FornecedoresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de fornecedores
     *
     * @return \Illuminate\Http\Response
     */
    public function list(FornecedorFilter $request)
    {

        $user = Auth::user();

        $fornecedores = Fornecedor::filter($request)
            ->whereHas('empresa', function($q) use ($user) {
                $q->whereIn('empresa_id', $user->empresas->pluck('id'));
            })->paginate(10);

        return view('fornecedores.list')
            ->with('fornecedores', $fornecedores);
    }

    /**
     * Mostra o formulário de cadastro de fornecedores
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $empresas = Auth::user()->hasRole('admin');

        return view('fornecedores.add')
            ->with('empresas', $empresas);
    }

    /**
     * Grava fornecedores no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(FornecedorRequest $request)
    {
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;

        Fornecedor::create($data);

        return redirect()->route('fornecedores.list')
            ->with('success', 'Fornecedor cadastrado com sucesso!');
    }

    /**
     * Mostra o formulário de editar fornecedores
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fornecedor = Fornecedor::find($id);

        return view('fornecedores.edit')
            ->with('fornecedor', $fornecedor);
    }

    /**
     * Grava fornecedores no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(FornecedorRequest $request)
    {
        $data = $request->input();
        $fornecedor = Fornecedor::find($data['id']);

        $fornecedor->fill($data);
        $fornecedor->save();

        return redirect()->route('fornecedores.list')
            ->with('success', 'Fornecedor alterado com sucesso!');
    }

    /**
     * Excluir a fornecedor
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $data = $request->input();
        $fornecedor = Fornecedor::find($data['id']);

        if ($fornecedor->despesas->count() || $fornecedor->receitas->count()) {
            return redirect()->route('fornecedores.list')
                ->with('error', 'Esse fornecedor está vinculado a uma receita ou despesa.');
        }

        $fornecedor->delete();

        return redirect()->route('fornecedores.list')
            ->with('success', 'Fornecedor excluído com sucesso');

    }

}
