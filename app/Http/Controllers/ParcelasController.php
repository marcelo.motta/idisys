<?php

namespace App\Http\Controllers;

use App\Despesa;
use App\Filters\ParcelaFilter;
use App\Parcela;
use App\Http\Requests\ParcelaRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class ParcelasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de parcelas
     *
     * @return \Illuminate\Http\Response
     */
    public function list(ParcelaFilter $request, Request $r)
    {
        $user = Auth::user();

        $parcelas = Parcela::filter($request)
            ->whereHas('despesa', function($d) use ($user){
                $d->whereHas('empresa', function($q) use ($user) {
                    $q->whereIn('empresa_id', $user->empresas->pluck('id'));
                })
                ->where('aprovada', 1);
            })
            ->paginate(10);

        $mes = $r->get('mes') ? Date::createFromFormat('m-Y', $r->get('mes')) : Date::now();

        return view('contasapagar.list')
            ->with('mes', $mes)
            ->with('parcelas', $parcelas);
    }

    /**
     * Mostra o formulário de parcelas
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('parcelas.add');
    }

    /**
     * Grava parcelas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ParcelaRequest $request)
    {
        $data = $request->input();
        $despesa = Despesa::find($data['despesa_id']);

        $data['empresa_id'] = Auth::user()->empresas->first()->id;
        $data['data']       = \Carbon\Carbon::createFromFormat('d/m/Y', $data['data'])->format('Y-m-d');
        $data['valor']      = str_replace('.', '', $data['valor']);
        $data['valor']      = str_replace(',', '.', $data['valor']);
        $data['situacao']   = 0;

        if (number_format($data['valor'], '2') > $despesa->despesa_parcelas_diferenca()){
            return redirect()->route('despesas.edit', $data['despesa_id'])
                ->with('error', 'O valor das parcelas não deve exceder o valor total da despesa!');
        }

        Parcela::create($data);

        return redirect()->route('despesas.edit', $data['despesa_id'])
            ->with('success', 'Parcela cadastrada com sucesso!');
    }

    /**
     * Mostra o formulário de editar item
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $item = Parcela::find($id);

        return view('parcelas.edit')
            ->with('item', $item);
    }

    /**
     * Grava item no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ParcelaRequest $request)
    {
        $data = $request->input();
        $item = Parcela::find($data['id']);

        $item->fill($data);
        $item->save();

        return redirect()->route('parcelas.list')
            ->with('success', 'Parcela alterado com sucesso!');
    }

    /**
     * Excluir item
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $parcela = Parcela::find($data['id']);
        $parcela->delete();

        return redirect()->route('despesas.edit', $parcela->despesa->id)
            ->with('success', 'Parcela excluído com sucesso');

    }

}
