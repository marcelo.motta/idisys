<!DOCTYPE html>
{{--<html class="no-js" lang="{{ config('app.locale') }}">--}}
<head>
    @include('layouts.shared.head')

    <link rel="stylesheet" href="css/_auth.scss">
</head>
<body>

    @yield('content')


    @include('layouts.shared.scripts')
</body>
</html>
