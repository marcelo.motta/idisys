<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="#">
            <img src="{{ asset('images/idisys.png') }}" alt="logo" />
        </a>

        {{--@TODO Logo para mobile--}}
        {{--<a class="navbar-brand brand-logo-mini" href="#">--}}
            {{--<img src="images/logo-mini.svg" alt="logo" />--}}
        {{--</a>--}}
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-right">

            {{--Carrega a lista de seleção de empresas para administrador--}}
            @role('admin')
            @if(Auth::user()->getEmpresa())
            <li class="nav-item dropdown d-none d-xl-inline-block">
                <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    <span class="profile-text"><i class="mdi mdi-domain"></i> {{ Auth::user()->getEmpresa()->fantasia }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">

                    @foreach(\App\Empresa::pluck('fantasia', 'id') as $id => $empresa)
                    <a class="dropdown-item mt-3" href="{{ route('users.setEmpresa', $id) }}">{{ $empresa }}</a>
                    @endforeach
                </div>
            </li>
            @endif
            @endrole

            <li class="nav-item dropdown d-none d-xl-inline-block">
                <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    <span class="profile-text">Olá, {{ Auth::user()->name }} !</span>
                    <img class="img-xs rounded-circle" src="{{ Auth::user()->photo() }}" alt="Profile image">
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">

                    <a class="dropdown-item mt-3" href="{{ route('users.password') }}">Alterar senha</a>
                    <a class="dropdown-item mt-3" href="{{ route('logout') }}" id="btnLoginYep"
                       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        Sair
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>
