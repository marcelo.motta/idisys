<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empresa_id',
        'nome',
        'cnpj',
        'contato',
        'telefone',
        'banco',
        'ag',
        'conta',
        'poupanca',
        'dados_nominais'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Define manualmente a tabela
     */
    protected $table = 'fornecedores';

    /**
     * Recebe a Empresa do Fornecedor
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    /**
     * Recebe as despesas do Fornecedor
     */
    public function despesas()
    {
        return $this->hasMany('App\Despesa');
    }

    /**
     * Recebe as receitas do Fornecedor
     */
    public function receitas()
    {
        return $this->hasMany('App\Receita');
    }

}
