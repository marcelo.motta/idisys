<?php

namespace App\Http\Controllers;

use App\Carteira;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarteirasController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   
        $name = $request->input('name');
        $user = Auth::user();

        $carteiras = Carteira::Where('empresa_id', $user->empresas->pluck('id'))->where('nome', 'like', '%' . $name . '%')->get();
        
        return view('carteiras.list')
            ->with('carteiras', $carteiras);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $empresas = Auth::user()->hasRole('admin');

        return view('carteiras.add')
            ->with('empresas', $empresas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;

        Carteira::create($data);

        return redirect()->route('carteiras.list')
            ->with('success', 'Carteira cadastrada com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carteira = Carteira::find($id);

        return view('carteiras.edit')
            ->with('carteira', $carteira);
    }

    /**
     * Grava Carteira no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->input();
        $carteira = Carteira::find($data['id']);

        $carteira->fill($data);
        $carteira->save();

        return redirect()->route('carteiras.list')
            ->with('success', 'Carteira alterada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $carteira = Carteira::find($data['id']);

        if ($carteira->despesas->count() || $carteira->receitas->count()) {
            return redirect()->route('carteiras.list')
                ->with('error', 'Esse centro de custos contém receita(s) ou despesa(s) vinculada(s).');
        }

        $carteira->delete();

        return redirect()->route('carteiras.list')
            ->with('success', 'Carteira excluída com sucesso');
    }
}
