@extends('layouts.app')

@section('content')

    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">EDITAR USUÁRIO</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('users.update') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <p class="card-description">
                        <b>DADOS DO USUÁRIO</b>
                    </p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NOME</label>
                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" value="{{ $user->name }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input name="email" type="text" class="form-control" value="{{ $user->email }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-sm-2 col-form-label">SENHA</label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<input name="password" type="password" class="form-control">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">FOTO</label>
                                <div class="col-sm-10">
                                    <input type="file" name="img" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" placeholder="Selecione uma foto">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info" type="button">Selecionar</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <p class="card-description">
                    <b>DADOS DE ACESSO</b>
                    </p>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">EMPRESA</label>
                                <div class="col-sm-10">
                                    {{ Form::select('empresa_id', $empresas, $user->empresas->pluck('id')->toArray(), ['class' => 'form-control select2', 'id' => 'empresa']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">PERMISSÃO</label>
                                <div class="col-sm-10">
                                    {{ Form::select('role_id', $roles, $user->roles->pluck('id')->toArray(), ['class' => 'form-control select2', 'id' => 'role']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-light btn-fw">Cancelar</a>
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
