<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empresa_id',
        'nome'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Define manualmente a tabela
     */
    protected $table = 'itens';

    /**
     * Recebe a Empresa do Fornecedor
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    /**
     * Recebe as despesas do Fornecedor
     */
    public function despesas()
    {
        return $this->belongsToMany('App\Despesa');
    }

}
