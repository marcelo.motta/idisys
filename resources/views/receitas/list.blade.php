@extends('layouts.app')

@section('content')
@role(['admin', 'manager'])
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-success">RECEITAS</button>
                        <a href="{{ route('receitas.add') }}" class="btn btn-success"><i class="mdi mdi-plus"></i> NOVA</a>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <form method="get" action="{{ route('receitas.list') }}">
                    <div class="input-group mb-3">
                        <input type="text" name="s" class="form-control" placeholder="Pesquisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <p class="card-description">

            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            FORNECEDOR
                        </th>
                        <th>
                            CENTRO DE CUSTO
                        </th>
                        <th>
                            DATA
                        </th>
                        <th>
                            VALOR
                        </th>
                        <th>
                            CARTEIRA
                        </th>
                        @role(['admin', 'manager'])
                        <th></th>
                        @endrole
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($receitas as $receita)
                            <tr>
                            <td class="col-lg-10">
                                {{ $receita->fornecedor->nome }}
                            </td>
                            <td class="col-lg-10">
                                @if ($receita->subcategoria)
                                {{ $receita->subcategoria->nome }}
                                @endif                               
                            </td>
                            <td class="col-lg-10">
                                {{ \Carbon\Carbon::parse($receita->data)->format('d/m/Y') }}
                            </td>
                            <td class="col-lg-10">
                                {{ number_format($receita->valor, 2, ',', '.') }}
                            </td>
                            <td class="col-lg-10">
                                @if ($receita->carteira)
                                {{ $receita->carteira->nome }}
                                @endif                               
                            </td>
                            @role(['admin', 'manager'])
                                <td class="text-right">
                                    <a href="{{ route('receitas.edit', $receita->id) }}" class="btn btn-outline-primary btn-rounded"><i class="fa fa-edit"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $receita->id }}" data-valor="{{ number_format($receita->valor, 2, ',', '.') }}" data-nome="{{ $receita->fornecedor->nome }}"  class="btn btn-outline-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endrole
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>

            {{ $receitas->links() }}
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir empresa')

        <span id="message">Deseja excluir a receita <strong></strong> de <span id="name"></span>?</span>

        <form method="post" action="{{ route('receitas.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
    @endrole
    @role('operator')
    <h3>É necassario um login adminitrativo para acessar essas informacões</h3>
    @endrole
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var nome = button.data('nome');
            var valor = button.data('valor');
            var modal = $(this);
            modal.find('.modal-body #name').append(" " + nome);
            modal.find('.modal-body #message strong').text(valor);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
