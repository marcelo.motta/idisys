<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReceitasDropAndSetColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receitas', function (Blueprint $table) {
            $table->dropForeign('fk_receitas_centrocustos1_idx');            
            $table->dropColumn('centrocusto_id');
            $table->integer('subcategoria_id')->unsigned();            
            $table->foreign('subcategoria_id')->references('id')->on('subcategorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receitas', function (Blueprint $table) {
            $table->integer('centrocusto_id')->unsigned();
            $table->index(["centrocusto_id"], 'fk_receitas_centrocustos1_idx');            
            $table->foreign('centrocusto_id', 'fk_receitas_centrocustos1_idx')
                ->references('id')->on('centro_custos')
                ->onDelete('no action')
                ->onUpdate('no action');
            $table->dropForeign('receitas_subcategoria_id_foreign');            
            $table->dropColumn('subcategoria_id'); 
        });
    }
}
