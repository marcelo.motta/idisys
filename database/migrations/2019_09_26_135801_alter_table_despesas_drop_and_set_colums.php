<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDespesasDropAndSetColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('despesas', function (Blueprint $table) {
            $table->dropForeign('fk_despesas_centro_custos1_idx');            
            $table->dropColumn('centro_custo_id');
            $table->integer('subcategoria_id')->unsigned();            
            $table->foreign('subcategoria_id')->references('id')->on('subcategorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('despesas', function (Blueprint $table) {
            $table->integer('centro_custo_id')->unsigned();
            $table->index(["centro_custo_id"], 'fk_despesas_centro_custos1_idx');
            $table->foreign('centro_custo_id', 'fk_despesas_centro_custos1_idx')
                ->references('id')->on('centro_custos')
                ->onDelete('no action')
                ->onUpdate('no action');
            $table->dropForeign('despesas_subcategoria_id_foreign');            
            $table->dropColumn('subcategoria_id');               
        });
    }
}
