<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class EmpresaFilter
 *
 * @package App\Filters
 */
class ReceitaFilter extends QueryFilters
{
	/**
	 * Do filter by name
	 *
	 * @param array $ids
	 *
	 * @return Builder
	 */
    public function s($s = '')
    {
		if($s != '')
			return $this->builder
                ->whereHas('fornecedor', function ($q) use ($s){
                    $q->where('nome', 'like', '%' . $s . '%');
                })
                ->orWhereHas('subcategoria', function ($q) use ($s){
                    $q->where('nome', 'like', '%' . $s . '%');
                })
                ->orWhereHas('carteira', function ($q) use ($s){
                    $q->where('nome', 'like', '%' . $s . '%');
                })
                ->orWhere('valor', 'like', '%'. $s .'%');


	    return $this->builder;
	}


}
