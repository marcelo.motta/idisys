const uploadButton = $('.file-upload-browse');
const realInput = $('.file-upload-default');
const fileInfo = $('.file-upload-info');

uploadButton.on('click', () => {
    realInput.click();
});

realInput.on('change', () => {
    const name = realInput.val().split(/\\|\//).pop();
    fileInfo.attr('placeholder', name);
});
