<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
        </li>
        <div class="btn-group">
            <button type="button" class="btn btn-success  dropdown-toggle cadastrar-novo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 75% !important; margin-top: 15px;">
                Cadastar novo
                <!--<i class="mdi mdi-plus"></i>-->
            </button>
            <div class="dropdown-menu">
                @role(['admin', 'manager'])
                <a class="dropdown-item" href="{{ route('receitas.add') }}">
                    <i class="menu-icon mdi mdi-trending-up"></i> Receita</a>
                @endrole
                <a class="dropdown-item" href="{{ route('despesas.add') }}">
                    <i class="menu-icon mdi mdi-trending-down"></i> Despesa</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('items.add') }}">
                    <i class="menu-icon mdi mdi-package-variant-closed"></i> Item</a>
                <a class="dropdown-item" href="{{ route('centros.add') }}">
                    <i class="menu-icon mdi mdi-loupe"></i> Centro de custo</a>
                <a class="dropdown-item" href="{{ route('carteiras.add') }}">
                    <i class="menu-icon mdi mdi-pocket"></i> Carteira</a>
                <a class="dropdown-item" href="{{ route('fornecedores.add') }}">
                    <i class="menu-icon mdi mdi-package-variant"></i> Fornecedor</a>
                @role('admin')
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('empresas.add') }}">
                    <i class="menu-icon mdi mdi-domain"></i> Empresa</a>
                <a class="dropdown-item" href="{{ route('users.add') }}">
                    <i class="menu-icon mdi mdi-account"></i> Usuário</a>
                @endrole
            </div>
        </div>
        <li class="nav-item {{ Route::is('dashboard') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="menu-icon mdi mdi-television"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        @role(['admin', 'manager'])
        <li class="nav-item">
            <a class="nav-link" href="{{ route('receitas.list') }}">
                <i class="menu-icon mdi mdi-trending-up"></i>
                <span class="menu-title">Receitas</span>
            </a>
        </li>
        @endrole
        <li class="nav-item">
            <a class="nav-link" href="{{ route('despesas.list') }}">
                <i class="menu-icon mdi mdi-trending-down"></i>
                <span class="menu-title">Despesas</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('contasapagar.list', ['mes' => \Carbon\Carbon::now()->format('m-Y')]) }}">
                <i class="menu-icon mdi mdi-cash"></i>
                <span class="menu-title">Contas a pagar</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('items.list') }}">
                <i class="menu-icon mdi mdi-package-variant-closed"></i>
                <span class="menu-title">Itens</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('centros.list') }}">
                <i class="menu-icon mdi mdi-loupe"></i>
                <span class="menu-title">Centro de Custo</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('carteiras.list') }}">
                <i class="menu-icon mdi mdi-pocket"></i>
                <span class="menu-title">Carteira</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('fornecedores.list') }}">
                <i class="menu-icon mdi mdi-package-variant"></i>
                <span class="menu-title">Fornecedores</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#report" aria-expanded="false" aria-controls="report">
                <i class="menu-icon mdi mdi-file-document-box"></i>
                <span class="menu-title">Relatórios</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="report">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('reports.contas.pagar', ["startDate" => \Carbon\Carbon::now()->startOfMonth()->format('d/m/Y'), "finalDate" => \Carbon\Carbon::now()->endOfMonth()->format('d/m/Y')]) }}">Contas a pagar </a>
                    </li>
                </ul>
            </div>
        </li>
        @role('admin')
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="menu-icon mdi mdi-wrench"></i>
                <span class="menu-title">Configurações</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('empresas.list') }}"><i class="menu-icon mdi mdi-domain"></i> Empresas </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.list') }}"><i class="menu-icon mdi mdi-account"></i> Usuários </a>
                    </li>
                </ul>
            </div>
        </li>
        @endrole
    </ul>
</nav>
