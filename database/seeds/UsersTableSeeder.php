<?php

use App\Role;
use App\User;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'      =>  "admin",
            'email'         =>  "marcelo.motta@gmail.com",
            'password'      =>  bcrypt("raposa"),
        ]);

        $admin->attachRole(Role::where('name', 'admin')->first());
    }
}
