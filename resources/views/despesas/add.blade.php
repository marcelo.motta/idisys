@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-danger">CADASTRO DE DESPESAS</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form id="despesasForm" class="form-sample" method="post" action="{{ route('despesas.store') }}">
                    @csrf

                    <p class="card-description">
                        <b>DADOS DA DESPESA</b>
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fornecedor</label>                                
                                {{ Form::select('fornecedor_id', $fornecedores, [], ['class' => 'form-control select2', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="numero">Número da nota</label>                               
                                <input class="form-control" name="numero" value="{{ old('numero') }}" required autocomplete="off">                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="data">Data emissão</label>                                
                                <input class="form-control datepicker" name="data" value="{{ old('data') }}" required autocomplete="off">                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Centro de Custo</label>
                                <select class="form-control select2" id="subcategoria_id" name="subcategoria_id">
                                    <option selected value=""></option>
                                    @foreach($centros as $centro)
                                        <optgroup label="{{$centro->nome}}">
                                            @foreach($centro->subcategorias as $subcategoria)                                            
                                                <option value="{{$subcategoria->id}}">{{$subcategoria->nome}}</option>                                                                                                                                
                                            @endforeach
                                        </optgroup>                                                                                        
                                    @endforeach
                                </select> 
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Forma de pagamento</label>
                                {{ Form::select('tipo_pagamento', ['dinheiro' => 'Dinheiro', 'conta' => 'Débito em conta', 'transferencia' => 'Transferência Bancária', 'boleto' => 'Boleto Bancário'], [], ['class' => 'form-control select2', 'required' => 'required', 'placeholder' => 'Selecione uma opção']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Carteira</label>
                                {{ Form::select('carteira_id', $carteiras, [], ['class' => 'form-control select2', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Observação</label>
                                <textarea rows="4" class="form-control" name="obs">{{ old('obs') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <p class="clearfix"></p>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" class="btn" value="Limpar">
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
