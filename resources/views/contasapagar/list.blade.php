@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-danger">CONTAS A PAGAR</button>
                        <a href="{{ route('despesas.add') }}" class="btn btn-danger"><i class="mdi mdi-plus"></i> NOVA</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group pull-right">
                        <div class="input-group">
                            <div class="input-group-prepend bg-primary border-primary">
                                <a href="{{ route('contasapagar.list', ['mes' => $mes->copy()->subMonth()->format('m-Y')]) }}" class="btn btn-danger"><i class="mdi  mdi-arrow-left-bold"></i></a>
                            </div>
                            <button class="btn btn-outline-danger">{{ $mes->format('F Y') }}</button>
                            <div class="input-group-append bg-primary border-primary">
                                <a href="{{ route('contasapagar.list', ['mes' => $mes->copy()->addMonth()->format('m-Y')]) }}" class="btn btn-danger"><i class="mdi  mdi-arrow-right-bold"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="card-description">
            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            FORNECEDOR
                        </th>
                        <th>
                            DOC
                        </th>
                        <th>
                            VENCIMENTO
                        </th>
                        <th>
                            VALOR
                        </th>
                        <th>
                            OBS
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($parcelas as $parcela)
                            <tr>
                            <td >{{ $parcela->despesa->fornecedor->nome }}</td>
                            <td >{{ $parcela->numero }}</td>
                            <td>
                                {{ \Carbon\Carbon::parse($parcela->data)->format('d/m/Y') }}
                            </td>
                            <td>
                                R$ {{ number_format($parcela->valor, 2, ',', '.') }}
                            </td>
                            <td class="col-md-3">
                                {{ $parcela->despesa->obs }}
                            </td>
                            <td class="text-right">
                                <a href="{{ route('despesas.edit', $parcela->despesa->id) }}" class="btn btn-outline-primary btn-rounded"><i class="fa fa-edit"></i></a>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>

            {{ $parcelas->links() }}
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir Item')

        <span id="message">Deseja excluir a empresa <strong></strong>?</span>

        <form method="post" action="{{ route('items.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $(this);
            modal.find('.modal-body #message strong').text(name);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            console.log($(this).parents().find('form'));
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
