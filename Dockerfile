FROM php:7.1-fpm
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN docker-php-ext-install pdo mbstring
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli

#FROM koutsoumpos89/composer-php7.1
#RUN php artisan key:generate
#RUN php artisan view:clear
#RUN php artisan cache:config
#RUN php artisan migrate
#RUN php artisan passport:install
