<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Filters\EmpresaFilter;
use App\Http\Requests\EmpresaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de empresas
     *
     * @return \Illuminate\Http\Response
     */
    public function list(EmpresaFilter $request)
    {
        $empresas = Empresa::filter($request)
            ->paginate(10);

        /**
         * Verifica se o usuário é admin e se ja tem uma empresa setada
         * Se não houver seta uma empresa
         */

        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }

        if($user->empresas->isEmpty() and $user->hasRole('admin')){

            // - Verifica se existem empresas cadastrada
            if (!$empresas->isEmpty()) {
                $user->setEmpresa($empresas->first());
            } else {
                return redirect()->route('empresas.add')
                    ->with('error', 'Por favor, cadastre uma empresa');
            }
        }

        return view('empresas.list')
            ->with('empresas', $empresas);
    }

    /**
     * Mostra o formulário de cadastro de empresas
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        return view('empresas.add');
    }

    /**
     * Grava empresas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaRequest $request)
    {
        $data = $request->input();

        Empresa::create($data);

        return redirect()->route('empresas.list')
            ->with('success', 'Empresa cadastrada com sucesso!');
    }

    /**
     * Mostra o formulário de editar empresas
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $empresa = Empresa::find($id);

        return view('empresas.edit')
            ->with('empresa', $empresa);
    }

    /**
     * Grava empresas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $request)
    {
        $data = $request->input();
        $empresa = Empresa::find($data['id']);

        $empresa->fill($data);
        $empresa->save();

        return redirect()->route('empresas.list')
            ->with('success', 'Empresa alterada com sucesso!');
    }

    /**
     * Excluir a empresa
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $empresa = Empresa::find($data['id']);

        if($empresa->users->isEmpty()) {

            $empresa->delete();

            return redirect()->route('empresas.list')
                ->with('success', 'Empresa excluído com sucesso');

        }
            return redirect()->route('empresas.list')
                ->with('error', 'Não foi possível excluir empresa. Existem itens vinculados');

    }

}
