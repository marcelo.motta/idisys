<?php

namespace App;

use App\Filters\Filterable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Storage;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Recebe a Empresa do usuário
     */
    public function empresas()
    {
        return $this->belongsToMany('App\Empresa');
    }

    /**
     * Recebe a Regra do usuário
     */
    public function role()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Recebe a foto do usuário
     * @return string
     */
    public function photo()
    {
        if ($this->photo and Storage::disk('public')->exists("users/{$this->photo}")) {
            return asset("storage/users/{$this->photo}");
        } else {
            return asset("images/avatar_default.png");
        }
    }

    /**
     * Deleta a foto do usuário
     * @return string
     */
    public function delete_photo(){
        if ($this->photo != null) {
            Storage::delete('users/' . $this->photo);
        }
    }

    /**
     * Deleta dependências do usuário e remove o mesmo
     * @return void
     */
    public function delete() {

        $this->empresas()->detach();
        $this->role()->detach();
        $this->delete_photo();

        parent::delete();
    }

    /**
     * Seta a empresa para o usuário
     * @return void
     */
    public function setEmpresa($empresa){
        $this->empresas()->sync($empresa);
    }

    /**
     * Recebe a empresa para o usuário
     * @return $empresa
     */
    public function getEmpresa(){
        return $this->empresas()->first();
    }
}
