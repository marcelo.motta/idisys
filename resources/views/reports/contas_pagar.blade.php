@extends('layouts.app')

@section('content')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">RELATÓRIO DE CONTAS A PAGAR</button>
                    </div>
                </div>
                <form action="">

                <div class="row mt-5">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="fornecedor">Fornecedor</label>
                          {{ Form::select('fornecedor_id', $fornecedores, [], ['class' => 'form-control select2', 'name' => 'fornecedor']) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="centro">Centro de Custo</label>
                            <select class="form-control select2" id="subcategoria_id" name="centro">
                                <option selected value=""></option>
                                @foreach($centros as $centro)
                                    <optgroup label="{{$centro->nome}}">
                                        @foreach($centro->subcategorias as $subcategoria)                                            
                                            <option value="{{$subcategoria->id}}">{{$subcategoria->nome}}</option>                                                                                                                                
                                        @endforeach
                                    </optgroup>                                                                                        
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="startDate">Data inicial</label>
                            <input type="text" class="form-control datepicker" id="startDate" name="startDate" placeholder="dd/mm/yyyy" value="{{ isset($filter['startDate']) ? $filter['startDate'] : \Carbon\Carbon::now()->startOfMonth()->format('d/m/Y') }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="finalDate">Data final</label>
                            <input type="text" class="form-control datepicker" id="finalDate" name="finalDate" placeholder="dd/mm/yyyy" value="{{ isset($filter['startDate']) ? $filter['finalDate'] : \Carbon\Carbon::now()->endOfMonth()->format('d/m/Y') }}">
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                        <a class="btn btn-primary" href="{{ route('reports.contas.pagar', ["startDate" => \Carbon\Carbon::now()->startOfMonth()->format('d/m/Y'), "finalDate" => \Carbon\Carbon::now()->endOfMonth()->format('d/m/Y')]) }}"><i class="fa fa-refresh"></i>Limpar Filtro</a>
                        
                    </div>
                </div>

                </form>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-body table-responsive">
                <table class="table mt-5">
                    <thead>
                        <tr>
                            <th>Situação</th>
                            <th>Nº do documento</th>
                            <th>Data</th>
                            <th>CNPJ</th>
                            <th>Fornecedor</th>
                            <th>NF</th>
                            <th>Observação</th>
                            <th>Contato</th>
                            <th>Telefone</th>                            
                            <th>Valor</th>
                            <th>Previsão de pagamento</th>
                            <th>Centro de custo</th>
                            <th>Carteira</th>
                            <th>Boleto</th>
                            <th>AG</th>
                            <th>C/C</th>
                            <th>C/P</th>
                            <th>Dados nominais</th>
                            <th>Banco</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contas as $conta)
                            <tr>
                                <td>{{ $conta->situacao ? 'PAGO' : 'EM ABERTO' }}</td>
                                <td>{{ $conta->numero }}</td>
                                <td>{{ $conta->data ? Carbon\Carbon::parse($conta->data)->format('d/m/Y') : '' }}</td>
                                <td>{{ $conta->despesa->fornecedor->cnpj }}</td>
                                <td>{{ $conta->despesa->fornecedor->nome }}</td>
                                <td>{{ $conta->despesa->fornecedor->nf }}</td>
                                <td>{{ $conta->despesa->obs }}</td>
                                <td>{{ $conta->despesa->fornecedor->contato }}</td>
                                <td>{{ $conta->despesa->fornecedor->telefone }}</td>                                
                                <td>{{ $conta->valor }}</td>
                                <td>{{ \Carbon\Carbon::parse($conta->data)->format('d/m/Y') }}</td>
                                <td>
                                    @if ($conta->despesa->subcategoria)
                                    {{ $conta->despesa->subcategoria->nome }}
                                    @endif
                                </td>
                                <td>
                                    @if ($conta->despesa->carteira)
                                    {{ $conta->despesa->carteira->nome }}
                                    @endif
                                </td>
                                <td>{{ $conta->despesa->boleto }}</td>
                                <td>{{ $conta->despesa->fornecedor->ag }}</td>
                                <td>{{ $conta->despesa->fornecedor->conta }}</td>
                                <td>{{ $conta->despesa->fornecedor->poupanca }}</td>
                                <td>{{ $conta->despesa->fornecedor->dados_nominais }}</td>
                                <td>{{ $conta->despesa->fornecedor->banco }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('table').DataTable({
                dom: 'Btp',
                "columnDefs": [
                    {
                        "targets": [ 2, 4, 6, 7, 10, 11, 12, 13, 14, 15, 16 ],
                        "visible": false,
                        "searchable": false
                    }
                    ],
                buttons: [
                    {
                        "extend": 'excelHtml5',
                        "title": 'Comercial {{ isset($filter["startDate"]) ? $filter["startDate"] : \Carbon\Carbon::now()->startOfMonth()->format("d/m/Y") }} a {{ isset($filter["startDate"]) ? $filter["finalDate"] : \Carbon\Carbon::now()->endOfMonth()->format("d/m/Y") }}',
                        "text":'<i class="fa fa-download"></i> Download',
                        "className": 'btn btn-primary btn-fw',
                        "init": function(api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        // "customize": function( xlsx ) {
                        //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        //     $('row c[r^="A1"]', sheet).attr( 's', '2' );
                        // },

                        autoFilter: true
                    },
                    {
                        "extend": 'colvis',
                        "className": 'btn btn-primary btn-fw',
                        "text": '<i class="fa fa-eye"></i> Exibir colunas',
                        collectionLayout: 'fixed two-column',
                        "init": function(api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                    }
                ],
                language: {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });


        } );
    </script>
@endpush
