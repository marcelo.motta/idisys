<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'razaosocial',
        'fantasia',
        'cnpj',
        'endereco',
        'cep',
        'telefone',
        'responsavel'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Recebe os usuários da Empresa
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Recebe os centros de custo da Empresa
     */
    public function centro_custos()
    {
        return $this->hasMany('App\CentroCusto');
    }

    /**
     * Recebe os fornecedores da Empresa
     */
    public function fornecedores()
    {
        return $this->hasMany('App\Fornecedor');
    }

    /**
     * Recebe as receitas da Empresa
     */
    public function receitas()
    {
        return $this->hasMany('App\Receita');
    }

    /**
     * Recebe as despesas da Empresa
     */
    public function despesas()
    {
        return $this->hasMany('App\Despesa');
    }

    /**
     * Retorna o valor total de receitas
     * @return int
     */
    public function receita_total($mes = null){
        $receitas = $this->receitas;

        if($mes) {
            $receitas = $receitas
                ->where('data', '>=', \Carbon\Carbon::createFromFormat('m-Y', $mes)->startOfMonth()->format('Y-m-d'))
                ->where('data', '<=', \Carbon\Carbon::createFromFormat('m-Y', $mes)->endOfMonth()->format('Y-m-d'));
        }
        return $receitas->sum('valor');
    }

    /** Retorna o valor total de despesas
     * @return int
     */
    public function despesa_total($mes = null){
        $total = 0;
        $despesas = $this->despesas;

        if ($mes){
            $despesas = $despesas
                ->where('data', '>=', \Carbon\Carbon::createFromFormat('m-Y', $mes)->startOfMonth()->format('Y-m-d'))
                ->where('data', '<=', \Carbon\Carbon::createFromFormat('m-Y', $mes)->endOfMonth()->format('Y-m-d'));
        }

        foreach ($despesas as $despesa) {
            $total = $total + $despesa->parcelas_total();
        }
        return $total;
    }

    /**
     * Retorna o saldo atual
     * @return int
     */
    public function saldo_atual(){
        return $this->receita_total() - $this->despesa_total();
    }

    /**
     * Retorna a quantidade total de lançamentos (receitas + despesas)
     * @return int
     */
    public function total_lancamentos($mes = null){

        $start_date = \Carbon\Carbon::createFromFormat('m-Y', $mes)->startOfMonth()->format('Y-m-d');
        $end_date = \Carbon\Carbon::createFromFormat('m-Y', $mes)->endOfMonth()->format('Y-m-d');

        $total_receitas = $this->receitas;
        $total_despesas = $this->despesas;

        if ($mes){
            $total_receitas = $total_receitas
                ->where('data', '>=', $start_date)
                ->where('data', '<=', $end_date);
            $total_despesas = $total_despesas
                ->where('data', '>=', $start_date)
                ->where('data', '<=', $end_date);
        }

        $total_receitas = $total_receitas->count();
        $total_despesas = $total_despesas->count();

        return $total_receitas + $total_despesas;
    }
}
