<?php

namespace App;


use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Receita extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empresa_id',
        'subcategoria_id',
        'carteira_id',
        'fornecedor_id',
        'data',
        'valor',
        'obs'
    ];

    /**
     * Make model filterable
     *
     * @see App\Filterable
     */
    use Filterable;

    /**
     * Recebe a Empresa
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    /**
     * Recebe a subcategoria do Centro de custo
     */
    public function subcategoria()
    {
        return $this->belongsTo('App\Subcategoria');
    }

    /**
     * Recebe a Carteira
     */
    public function carteira()
    {
        return $this->belongsTo('App\Carteira');
    }

    /**
     * Recebe o Fornecedor
     */
    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedor');
    }



}
