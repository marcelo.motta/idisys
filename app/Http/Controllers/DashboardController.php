<?php

namespace App\Http\Controllers;

use App\Carteira;
use App\CentroCusto;
use App\Subcategoria;
use App\Despesa;
use App\DespesaItem;
use App\Empresa;
use App\Parcela;
use App\Item;
use App\Receita;
use App\Filters\ParcelaFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {

        /**
         * Verifica se o usuário é admin e se ja tem uma empresa setada
         * Se não houver seta uma empresa
         */

        
        $user = Auth::user();
        $empresa = Empresa::first();

        if($user->empresas->isEmpty() and $user->hasRole('admin')){

            // - Verifica se existem empresas cadastrada
            if ($empresa) {
                $user->setEmpresa($empresa);
            } else {
                return redirect()->route('empresas.add')
                    ->with('error', 'Por favor, cadastre uma empresa');
            }
        }
        
        /**
         * Carrega a empresa atual do usuário
         */
        $empresa_atual = Empresa::whereHas('users', function ($q) use ($user){
            $q->where('id', $user->id);
        })->first();
        
        /**
         * Gera dados para alimentar os graficos de receitas e despesas do mes
         */

        $start_date = \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d');
        $end_date = \Carbon\Carbon::now()->endOfMonth()->format('Y-m-d');
        
        $despesas_atencao = DespesaItem::whereHas('despesa', function ($d) use ($empresa_atual, $start_date, $end_date){
                $d->where('empresa_id', $empresa_atual->id)
                    ->where('data', '>=', $start_date)
                    ->where('data', '<=', $end_date);
            })->where('atencao', 1)
            ->get();
          
           
        /**
         * DESPESAS
         */
               
        $centros_despesas = CentroCusto::where('empresa_id', $empresa_atual->id)
            ->whereHas('subcategorias.despesas', function ($q) use ($start_date, $end_date){
                $q->where('aprovada', 1)
                    ->where('data', '>=', $start_date)
                    ->where('data', '<=', $end_date);

            })
            ->get();       
          
        $despesas_centro = [];
            
        foreach ($centros_despesas as $k => $centro){
            $subcategorias = Subcategoria::where('centro_custo_id', $centro->id)->get();
            $valor_despesa = null;

            foreach ($subcategorias as $subcategoria) {
                
                $valor_despesa += $subcategoria->despesa_total($start_date, $end_date);
                
            }
     
            $color = implode(',', dynamicColors());

            $despesas_centro['title'][$k] = $centro->nome;
           // $despesas_centro['valor'][$k] = $centro->despesa_total($start_date, $end_date);
            $despesas_centro['valor'][$k] = $valor_despesa;
            $despesas_centro['bg'][$k] = 'rgba(' . $color . ', 0.5)';
            $despesas_centro['border'][$k] = 'rgba(' . $color . ', 0.75)';
        }
       
        
        if ($despesas_centro) {
            $despesas_centro_chart = [
                "datasets" => [
                    [
                        "data" => $despesas_centro['valor'],
                        "backgroundColor" => $despesas_centro['bg'],
                        "borderColor" => $despesas_centro['border'],
                    ]
                ],
                "labels" => $despesas_centro['title']
            ];
        }
        
        /**
         * RECEITAS
         */
        $centros_receitas = CentroCusto::where('empresa_id', $empresa_atual->id)
            ->whereHas('subcategorias.receitas', function ($q) use ($start_date, $end_date){
                $q->where('data', '>=', $start_date)
                    ->where('data', '<=', $end_date);

            })
            ->get();
        

        $receitas_centro = [];

        foreach ($centros_receitas as $k => $centro){

            $subcategorias = Subcategoria::where('centro_custo_id', $centro->id)->get();
            $valor_receita = null;

            foreach ($subcategorias as $subcategoria) {
                
                $valor_receita += $subcategoria->receita_total($start_date, $end_date);
                
            }
            
            $color = implode(',', dynamicColors());

            $receitas_centro['title'][$k] = $centro->nome;
            $receitas_centro['valor'][$k] = "$valor_receita";
            $receitas_centro['bg'][$k] = 'rgba(' . $color . ', 0.5)';
            $receitas_centro['border'][$k] = 'rgba(' . $color . ', 0.75)';
        }
        
        if ($receitas_centro) {
            $receitas_centro_chart = [
                "datasets" => [
                    [
                        "data" => $receitas_centro['valor'],
                        "backgroundColor" => $receitas_centro['bg'],
                        "borderColor" => $receitas_centro['border'],
                    ]
                ],
                "labels" => $receitas_centro['title']
            ];
        }
        
        /**
         * Exibe despeza x receita por centro de custo
         */

        $centros_custo_id = $request->input('centro_id');        
        
        $centros_custo = CentroCusto::where('empresa_id', $empresa_atual->id)
            ->whereHas('subcategorias.receitas')
            ->orWhereHas('subcategorias.despesas');        
                           
        $centros_custo_list = Clone $centros_custo;
        $centros_custo_list = $centros_custo_list->where('empresa_id', $empresa_atual->id)->pluck('nome', 'id');

        if ($centros_custo_id){
            $aux4 = CentroCusto::where('empresa_id', $empresa_atual->id)->find($centros_custo_id); 
            $centro_custo_selected = $aux4->id;           
        } else {
            $aux3 = $centros_custo->first();
            if( $aux3 ){
                $centro_custo_selected = $aux3->id;
            } else {
                $centro_custo_selected = null;
            }        
        }
        //dd($centro_custo_selected);
        
        $subs = Subcategoria::where('centro_custo_id', $centro_custo_selected)->get();
        $despesa_valor = 0;
        $receita_valor = 0;

        foreach ($subs as $sub){

            $despesas = Despesa::where('empresa_id', $empresa_atual->id)->where('subcategoria_id', $sub->id)
            ->where('data', '>=', $start_date)
            ->where('data', '<=', $end_date)->get();
            $receitas = Receita::where('empresa_id', $empresa_atual->id)->where('subcategoria_id', $sub->id)
            ->where('data', '>=', $start_date)
            ->where('data', '<=', $end_date)->get();
           

            foreach ($despesas as $despesa) {
                $despesa_valor = $despesa_valor + $despesa->parcelas_total();
            }
           
            foreach ($receitas as $receita) {                
                $receita_valor += $receita->valor;                
            }
           
        }

        if ($receita_valor <> 0 || $despesa_valor <> 0){

            $despesas_receitas_centro_chart = [
                "datasets" => [
                    [
                        "data" => [$receita_valor, $despesa_valor],
                        "backgroundColor" => [
                            'rgba(75, 192, 192, 0.5)',
                            'rgba(255, 99, 132, 0.5)'
                        ],
                        "borderColor" => [
                            'rgba(75, 192, 192,1)',
                            'rgba(255,99,132,1)'
                        ]
                    ]
                ],
                "labels" => ['Receitas', 'Despesas']
            ];
        }

       /**
         * Exibe despeza x receita por carteira
         */
        
        $carteira_id = $request->input('carteira_id');

        $carteira = Carteira::where('empresa_id', $empresa_atual->id)
            ->whereHas('receitas')
            ->orWhereHas('despesas');

        $carteira_list = Clone $carteira;
        $carteira_list = $carteira_list->where('empresa_id', $empresa_atual->id)->pluck('nome', 'id');
        
        
        if ($carteira_id){
            
            $aux = Carteira::where('empresa_id', $empresa_atual->id)->find($carteira_id);            
            $carteira_selected = $aux->id;

        } else {
            $aux2 = $carteira->where('empresa_id', $empresa_atual->id)->first();
            
            if( $aux2 ){
                $carteira_selected = $aux2->id;
            } else {
                $carteira_selected = null;
            }
        }

       
        $carteira_receitas = Receita::where('carteira_id', $carteira_selected)
            ->where('data', '>=', $start_date)
            ->where('data', '<=', $end_date)->get();
        $carteira_despesas = Despesa::where('carteira_id', $carteira_selected)
            ->where('data', '>=', $start_date)
            ->where('data', '<=', $end_date)->get();
            
        foreach ($carteira_despesas as $despesa){
            $despesa->valor = $despesa->parcelas_total();
        }

        if (!$carteira_receitas->isEmpty() || !$carteira_despesas->isEmpty()){

            $despesas_receitas_carteira_chart = [
                "datasets" => [
                    [
                        "data" => [$carteira_receitas->sum('valor'), $carteira_despesas->sum('valor')],
                        "backgroundColor" => [
                            'rgba(75, 192, 192, 0.5)',
                            'rgba(255, 99, 132, 0.5)'
                        ],
                        "borderColor" => [
                            'rgba(75, 192, 192,1)',
                            'rgba(255,99,132,1)'
                        ]
                    ]
                ],
                "labels" => ['Receitas', 'Despesas']
            ];
        }
       //filtra e envia a tabela de contas a pagar
        $table = Parcela::orderBy('data','asc')
        ->whereHas('despesa', function($d) use ($empresa_atual){
            $d->whereHas('empresa', function($q) use ($empresa_atual) {
                $q->whereIn('empresa_id', $empresa_atual->pluck('id'));
            })
            ->where('aprovada', 1);
        })->where('data', '>=', $start_date)
        ->where('data', '<=', $end_date)
        ->limit(6)->get();


        return view('dashboard')
            ->with('empresa_atual', $empresa_atual)
            ->with('centros_custo', $centros_custo_list)
            ->with('carteira', $carteira_list)
            ->with('table', $table)
            ->with('despesas_atencao', $despesas_atencao)
            ->with('centro_custo_selected', $centro_custo_selected)
            ->with('carteira_selected', $carteira_selected)
            ->with('despesas_receitas_centro_chart', isset($despesas_receitas_centro_chart) ? $despesas_receitas_centro_chart : null)
            ->with('despesas_receitas_carteira_chart', isset($despesas_receitas_carteira_chart) ? $despesas_receitas_carteira_chart : null)
            ->with('despesas_centro_chart', isset($despesas_centro_chart) ? $despesas_centro_chart : '')
            ->with('receitas_centro_chart', isset($receitas_centro_chart) ? $receitas_centro_chart : '');
    }


    
}
