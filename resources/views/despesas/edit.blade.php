@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-danger">EDITAR DESPESA</button>
                    </div>
                    <div class="col-lg-6 text-right">
                        <span class="text-{{ $despesa->aprovada ? 'success' : 'danger' }}">{!! $despesa->aprovada ? '<i class="mdi mdi-check"></i> Aprovada' : '<i class="mdi mdi-close"></i> Não aprovada' !!}</span>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('despesas.update') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $despesa->id }}">
                    <p class="card-description">
                        <b>DADOS DA DESPESA</b>
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Fornecedor</label>
                                <div class="col-sm-9">
                                    {{ Form::select('fornecedor_id', $fornecedores, $despesa->fornecedor->id, ['class' => 'form-control select2', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Número da nota</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="numero" value="{{ $despesa->numero }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Data emissão</label>
                                <div class="col-sm-9">
                                    <input class="form-control datepicker" name="data" value="{{ \Carbon\Carbon::parse($despesa->data)->format('d/m/Y') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Valor R$</label>
                                <div class="col-sm-9">
                                    <input type="text" name="valor" value="{{ number_format($despesa->valor(), 2, ',', '.') }}" class="form-control money" required  data-affixes-stay="true" data-thousands="." data-decimal=","  readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Forma de pagamento</label>
                                <div class="col-sm-9">
                                    {{ Form::select('tipo_pagamento', ['dinheiro' => 'Dinheiro', 'transferencia' => 'Transferência Bancária', 'conta' => 'Débito em conta', 'boleto' => 'Boleto Bancário'], $despesa->tipo_pagamento, ['class' => 'form-control select2', 'required' => 'required', 'placeholder' => 'Selecione uma opção']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Centro de Custo</label>
                                <div class="col-sm-9">
                                <select class="form-control select2" id="subcategoria_id" name="subcategoria_id">
                                    @if ($despesa->subcategoria)
                                    <option value="{{$despesa->subcategoria->id}}" selected>{{$despesa->subcategoria->nome}}</option>
                                    @endif
                                    @foreach($centros as $centro)
                                        <optgroup label="{{$centro->nome}}">
                                            @foreach($centro->subcategorias as $subcategoria)
                                                <option value="{{$subcategoria->id}}">{{$subcategoria->nome}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Carteira</label>
                                <div class="col-sm-9">
                                    {{ Form::select('carteira_id', $carteiras, $despesa->carteira_id, ['class' => 'form-control select2', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Observação</label>
                                <textarea rows="6" class="form-control" name="obs">{{ $despesa->obs }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dados p/ pagamento</label>
                                <textarea rows="6" class="form-control">
Dados nomimais: {{ $despesa->fornecedor->dados_nominais }}
CNPJ: {{ $despesa->fornecedor->cnpj }}
Banco: {{ $despesa->fornecedor->banco }}
Agência: {{ $despesa->fornecedor->ag }}
Conta: {{ $despesa->fornecedor->conta }}
Poupança: {{ $despesa->fornecedor->poupanca }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                    @if(!$despesa->aprovada)
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="submit" class="btn btn-success" value="Alterar despesa">
                        </div>
                    </div>
                    @endif
                </form>

                <hr>


                @if($despesa->despesa_parcelas_diferenca())
                <form class="form-sample" method="post" action="{{ route('despesas.parcelas.store') }}">
                    @csrf
                    <input type="hidden" name="despesa_id" value="{{ $despesa->id }}">

                    <p class="card-description">
                        <b>PARCELAS</b>
                    </p>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Valor R$</label>
                                <input type="text" name="valor" class="form-control money" required  data-affixes-stay="true" data-thousands="." data-decimal=",">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Número do documento</label>
                                <input type="text" name="numero" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Vencimento</label>
                                <input type="text" name="data" class="form-control datepicker" autocomplete="off" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @if($despesa->despesa_parcelas_diferenca())
                                <span class="text-danger">Restam {{ number_format(is_integer($despesa->despesa_parcelas_diferenca()) ? $despesa->despesa_parcelas_diferenca() : 0, 2, ',', '.') }} a serem lançados</span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-success"><i class="mdi mdi-plus"></i> Adicionar parcela</button>
                        </div>
                    </div>

                </form>

                @endif
                <p class="card-description">
                    <b>PARCELAS</b>
                </p>
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <table id="item" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th >Número do documento</th>
                                <th >Valor</th>
                                @if(!$despesa->aprovada)
                                <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if($despesa->parcelas->isEmpty())
                                <tr id="empty">
                                    <td colspan="4" class="text-warning text-center">
                                        <strong>Nenhuma parcela ainda</strong>
                                    </td>
                                </tr>
                            @else
                                @foreach($despesa->parcelas as $parcela)
                                    <tr>
                                        <td >{{ \Carbon\Carbon::parse($parcela->data)->format('d/m/Y') }}</td>
                                        <td >{{$parcela->numero}}</td>
                                        <td >R$ {{ number_format($parcela->valor, 2, ',', '.') }}</td>
                                        @if(!$despesa->aprovada)
                                        <td class="text-right col-lg-3">
                                            <button class="btn btn-danger btn-rounded btnDelItem" data-toggle="modal" data-target="#modalExcluirParcela"
                                                    data-id="{{ $parcela->id }}" data-valor="{{ number_format($parcela->valor, 2, ',', '.') }}">
                                                <i class="mdi mdi-delete"></i> Excluir
                                            </button>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <hr>

                @if(!$despesa->aprovada)
                <form class="form-sample" method="post" action="{{ route('despesas.item.add') }}">
                    @csrf
                    <input type="hidden" name="despesa_id" value="{{ $despesa->id }}">

                    <p class="card-description">
                        <b>ADICIONAR ITENS</b>
                    </p>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Item</label>
                                <div class="col-sm-9">
                                    {{ Form::select('item_id', $itens, [], ['class' => 'form-control select2', 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Valor R$</label>
                                <div class="col-sm-9">
                                    <input type="text" name="valor" class="form-control money" required  data-affixes-stay="true" data-thousands="." data-decimal=",">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" name="atencao" class="form-check-input"> Este item requer atenção?
                                    <i class="input-helper"></i></label>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="submit" class="btn btn-success" value="Adicionar item">
                        </div>
                    </div>

                    <hr>
                    @endif

                    <p class="card-description">
                        <b>ITENS ADICIONADOS</b>
                    </p>
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <table id="item" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="text-right">Valor</th>
                                    <th class="text-right">Atenção</th>
                                    @if(!$despesa->aprovada)
                                    <th></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @if($despesa->itens->isEmpty())
                                    <tr id="empty">
                                        <td colspan="4" class="text-warning text-center">
                                            <strong>Nenhum item adicionado</strong>
                                        </td>
                                    </tr>
                                @else
{{--                                    @dd($despesa->itens)--}}
                                    @foreach($despesa->itens as $item)
                                        <tr>
                                            <td class="col-lg-8">{{ $item->nome }}</td>
                                            <td class="text-right col-lg-2">R$ {{ number_format($item->pivot->valor, 2, ',', '.') }}</td>
                                            <td class="text-right col-lg-1">{{ $item->pivot->atencao ? 'Sim' : 'Não' }}</td>
                                            @if(!$despesa->aprovada)
                                            <td class="text-right col-lg-1">
                                                <button class="btn btn-danger btn-rounded btnDelItem" data-toggle="modal" data-target="#modalExcluir"
                                                        data-nome="{{ $item->nome }}" data-item_id="{{ $item->id }}" data-valor="{{ $item->pivot->valor }}">
                                                    <i class="mdi mdi-delete"></i> Excluir
                                                </button>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
                <hr>
                @role('admin')
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-{{ $despesa->aprovada ? 'danger' : 'success' }}" data-toggle="modal" data-target="#modalAprovar" data-id="{{ $despesa->id }}">{{ $despesa->aprovada ? 'Reprovar' : 'Aprovar' }} despesa</button>
                    </div>
                </div>
                @endrole

            </div>
        </div>
        @component('shared._modal')

            @slot('modal', 'modalExcluir')
            @slot('title', 'Excluir Item')

            <span id="message">Deseja excluir o item <strong></strong>?</span>

            <form method="post" action="{{ route('despesas.item.delete') }}">
                @csrf
                <input type="hidden" name="despesa_id" id="despesa_id" value="{{ $despesa->id }}">
                <input type="hidden" name="item_id" id="item_id">
                <input type="hidden" name="valor" id="valor">
            </form>

            @slot('footer')
                <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
                <input type="submit" class="btn btn-danger" value="Excluir">
            @endslot


        @endcomponent
        @component('shared._modal')

            @slot('modal', 'modalExcluirParcela')
            @slot('title', 'Excluir parcela')

            <span id="message">Deseja excluir a parcela de <strong></strong>?</span>

            <form method="post" action="{{ route('despesas.parcelas.delete') }}">
                @csrf
                <input type="hidden" name="despesa_id" id="despesa_id" value="{{ $despesa->id }}">
                <input type="hidden" name="id" id="id">
            </form>

            @slot('footer')
                <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
                <input type="submit" class="btn btn-danger" value="Excluir">
            @endslot


        @endcomponent
        @component('shared._modal')

            @slot('modal', 'modalAprovar')
            @slot('title', $despesa->aprovada ? 'Reprovar despesa' : 'Aprovar despesa')

            <span id="message">Deseja {{ $despesa->aprovada ? 'reprovar' : 'aprovar' }} essa despesa?</span>

            <form method="post" action="{{ route('despesas.aprovar') }}">
                @csrf
                <input type="hidden" name="id" id="id">
            </form>

            @slot('footer')
                <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
                <input type="submit" class="btn btn-{{ $despesa->aprovada ? 'danger' : 'success' }}" value="{{ $despesa->aprovada ? 'Reprovar' : 'Aprovar' }}">
            @endslot


        @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var item_id = button.data('item_id');
            var nome = button.data('nome');
            var valor = button.data('valor');
            var modal = $(this);
            modal.find('.modal-body #message strong').append(" " + nome);
            modal.find('.modal-body #item_id').val(item_id);
            modal.find('.modal-body #valor').val(valor);
        });

        $('#modalExcluirParcela').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var valor = button.data('valor');
            var modal = $(this);
            modal.find('.modal-body #message strong').text(" R$ " + valor);
            modal.find('.modal-body #id').val(id);

        });

        $('#modalAprovar').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var modal = $(this);
            modal.find('.modal-body #id').val(id);

        });

        $('#modalExcluir input[type=submit]').on('click', function () {
            $('#modalExcluir').find('form').submit();
        });
        $('#modalExcluirParcela input[type=submit]').on('click', function () {
            $('#modalExcluirParcela').find('form').submit();
        });
        $('#modalAprovar input[type=submit]').on('click', function () {
            $('#modalAprovar').find('form').submit();
        });
    </script>
@endpush
