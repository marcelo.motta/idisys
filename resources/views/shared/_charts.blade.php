@if($data)

    <canvas id="{{ $chart }}" style="height: 278px; display: block; width: 556px;" width="556" height="278" class="chartjs-render-monitor"></canvas>

    @push('scripts')
    <script>
        if ($("#{{ $chart }}").length) {
            var doughnutChartCanvas = $("#{{ $chart }}").get(0).getContext("2d");
            var doughnutChart = new Chart(doughnutChartCanvas, {
                type: 'doughnut',
                data: {!!  json_encode($data) !!},
                options: {
                    responsive: true,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
        }
    </script>
    @endpush

@else

    <div class="alert alert-warning text-center mt-5 text-small">
        Não possuem receitas e/ou despesas para esse centro de custo
    </div>

@endif
