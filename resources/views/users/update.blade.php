@extends('layouts.app')

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-outline-primary">ALTERAR SENHA</button>
                    </div>
                </div>

                <p class="card-description">

                </p>
                <form class="form-sample" method="post" action="{{ route('users.password.update') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">SENHA</label>
                                <div class="col-sm-10">
                                    <input name="password" type="password" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">CONFIRMAR SENHA</label>
                                <div class="col-sm-10">
                                    <input name="password_confirmation" type="password" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" class="btn" value="Limpar">
                            <input type="submit" class="btn btn-success" value="Gravar">
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection
