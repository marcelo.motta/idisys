<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carteira extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome','empresa_id'
    ];

    /**
     * Recebe as Receitas
     */
    public function receitas()
    {
        return $this->hasMany('App\Receita');
    }

    /**
     * Recebe as Despesas
     */
    public function despesas()
    {
        return $this->hasMany('App\Despesa');
    }



}
