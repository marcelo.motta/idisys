<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserFilter
 *
 * @package App\Filters
 */
class UserFilter extends QueryFilters
{
	/**
	 * Do filter by name
	 *
	 * @param array $ids
	 *
	 * @return Builder
	 */
    public function name($name = '')
    {
		if($name != '')
			return $this->builder
                ->where('name', 'like', '%' . $name . '%')
				->orWhere('email', 'like', '%' . $name . '%');


	    return $this->builder;
	}


}
