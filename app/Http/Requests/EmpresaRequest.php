<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razaosocial' => 'required',
            'fantasia' => 'required',
            'cnpj' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'razaosocial.required' => 'O campo razão social é obrigatório',
            'fantasia.required' => 'O campo nome fantasia é obrigatório',
            'cnpj.required' => 'O campo cnpj é obrigatório',
        ];
    }
}
