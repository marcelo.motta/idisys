<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'email|unique:users|required',
            'password' => ['required', 'string', 'min:6'],
            'empresa_id' => 'required',
            'role_id' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'email.email' => 'O campo email deve ser um endereço válido',
            'email.unique' => 'O endereço email de email ja existe no sistema',
            'password.required' => 'O campo password é obrigatório',
            'password.string' => 'O campo password precisa ser do tipo string',
            'password.min' => 'O campo password precisa ter pelo menos 6 caracteres',
            'empresa_id.required' => 'O campo empresa é obrigatório',
            'role_id.required' => 'O campo permissão é obrigatório'
        ];
    }
}
