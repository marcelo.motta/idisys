<?php

namespace App\Http\Controllers;

use App\CentroCusto;
use App\Carteira;
use App\Fornecedor;
use App\Despesa;
use App\Filters\DespesaFilter;
use App\Http\Requests\DespesaRequest;
use App\Item;
use App\Subcategoria;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DespesasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostra a lista de receitas
     *
     * @return \Illuminate\Http\Response
     */
    public function list(DespesaFilter $request)
    {
        $user = Auth::user();
        $despesas = Despesa::filter($request)
            ->whereHas('empresa', function($q) use ($user) {
                $q->whereIn('empresa_id', $user->empresas->pluck('id'));
            })->paginate(10);

        return view('despesas.list')
            ->with('despesas', $despesas);
    }

    /**
     * Mostra o formulário de cadastro de despesas
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $user = Auth::user();
        $fornecedores = Fornecedor::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        $centros = CentroCusto::where('empresa_id', $user->empresas->pluck('id'))->get();
        $carteiras = Carteira::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        
        
        return view('despesas.add')
            ->with('fornecedores', $fornecedores)
            ->with('centros', $centros)
            ->with('carteiras', $carteiras);
           
    }

    /**
     * Grava despesa no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DespesaRequest $request)
    {   
        $data = $request->input();

        $data['empresa_id'] = Auth::user()->empresas->first()->id;
        $data['data'] = Carbon::createFromFormat('d/m/Y', $data['data'])->format('Y-m-d');
        $data['aprovada'] = 0;

        $despesa = Despesa::create($data);

        return redirect()->route('despesas.edit', $despesa->id)
            ->with('success', 'Despesa cadastrada com sucesso! Agora adicione os itens.');
    }

    /**
     * Mostra o formulário de editar despesas
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $despesa = Despesa::with(['parcelas' => function ($p){
            $p->orderBy('data', 'asc');
        }])->find($id);
        $fornecedores = Fornecedor::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');
        $centros = CentroCusto::where('empresa_id', $user->empresas->pluck('id'))->get();
        $carteiras = Carteira::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');


        // @TODO Colocar para não listar itens que ja foram adicionados na despesa
        $itens = Item::where('empresa_id', $user->empresas->pluck('id'))->pluck('nome', 'id')->prepend('Selecione uma opção', '');

        return view('despesas.edit')
            ->with('despesa', $despesa)
            ->with('fornecedores', $fornecedores)
            ->with('itens', $itens)
            ->with('carteiras', $carteiras)
            ->with('centros', $centros);
    }

    /**
     * Grava despesas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DespesaRequest $request)
    {
        $data = $request->input();
        $despesa = Despesa::find($data['id']);
        $data['data'] = Carbon::createFromFormat('d/m/Y', $data['data'])->format('Y-m-d');

        $despesa->fill($data);
        $despesa->save();

        return redirect()->route('despesas.list')
            ->with('success', 'Despesa alterada com sucesso!');
    }


    /**
     * Grava os itens da despesas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function itemAdd(Request $request)
    {
        $data = $request->input();
        $despesa = Despesa::find($data['despesa_id']);

        if(!$data['item_id']){
            return redirect()->route('despesas.edit', $despesa->id)
                ->with('error', 'Nenhum item foi cadastrado!');
        }
        $item['atencao'] = isset($data['atencao']) ? 1 : 0;
        $item['valor'] = str_replace('.', '', $data['valor']);
        $item['valor'] = str_replace(',', '.', $item['valor']);

        if(! $despesa->itens->contains($data['item_id'])) {
            $despesa->itens()->attach($data['item_id'], $item);
        } else {
            return redirect()->route('despesas.edit', $despesa->id)
                ->with('error', 'Item já está adicionado na despesa!');
        }
        return redirect()->route('despesas.edit', $despesa->id)
            ->with('success', 'Item adicionado com sucesso!');
    }

    /**
     * Exclui os itens da despesas no banco de dados
     *
     * @return \Illuminate\Http\Response
     */
    public function itemDel(Request $request){
        $data = $request->input();

        $despesa = Despesa::find($data['despesa_id']);

        $despesa->itens()->detach($data['item_id'], ['valor'=>$data['valor']]);

        return redirect()->route('despesas.edit', $despesa->id)
            ->with('success', 'Item removido com sucesso!');

    }

    /**
     * Excluir a despesa
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $despesa = Despesa::find($data['id']);

        if($despesa->delete()) {

            return redirect()->route('despesas.list')
                ->with('success', 'Despesas excluído com sucesso');

        }
        return redirect()->route('despesas.list')
            ->with('error', 'Não foi possível excluir essa despesa. Por favor tente novamente.');

    }

    /**
     * Aprovar a despesa
     *
     * @return \Illuminate\Http\Response
     */
    public function aprovar(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin', 'manager'])){
            return redirect()->back()
                ->with('error', 'Você não tem permissão para acessar essa página');
        }
        $data = $request->input();
        $despesa = Despesa::find($data['id']);

        if ($despesa->despesa_parcelas_diferenca() > 0){
            return redirect()->route('despesas.edit', $despesa->id)
                ->with('error', 'Existe ainda valores a serem lançados em parcelas!');
        }
        if ($despesa->valor() == 0){
            return redirect()->route('despesas.edit', $despesa->id)
                ->with('error', 'Não existem itens lançados nesta despesa!');
        }

        $data['aprovada'] = !$despesa->aprovada;
        $despesa->fill($data);
        $despesa->save();


        return redirect()->route('despesas.edit', $despesa->id)
            ->with('success', $despesa->aprovada ? 'Despesas aprovada com sucesso' : 'Despesas reprovada com sucesso');
    }

}
