@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-danger">DESPESAS</button>
                        <a href="{{ route('despesas.add') }}" class="btn btn-danger"><i class="mdi mdi-plus"></i> NOVA</a>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <form method="get" action="{{ route('despesas.list') }}">
                    <div class="input-group mb-3">
                        <select name="status" id="inputState" class="form-control mr-3">
                             <option selected value="">Todos</option>
                             <option value="1">Aprovada</option>
                             <option value="0" >Não aprovada</option>
                         </select>
                        <input type="text" name="s" class="form-control" placeholder="Pesquisar" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                        </div>
                        
                    </div>
                    </form>
                </div>
            </div>
            <p class="card-description">

            </p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            FORNECEDOR
                        </th>
                        <th>
                            CENTRO DE CUSTO
                        </th>                        
                        <th>
                            DATA
                        </th>
                         <th>
                            NÚMERO DA NOTA
                        </th>
                        <th>
                            VALOR
                        </th>                       
                        <th>
                            CARTEIRA
                        </th>                       
                        <th>
                            SITUAÇÃO
                        </th>
{{--                        @role(['admin', 'manager'])--}}
                        <th></th>
{{--                        @endrole--}}
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($despesas as $despesa)
                            <tr>
                            <td class="col-lg-10">
                                {{ $despesa->fornecedor->nome }}
                            </td>
                            <td class="col-lg-10">
                                @if ($despesa->subcategoria)
                                {{ $despesa->subcategoria->nome }}
                                @endif 
                            </td>
                            <td class="col-lg-10">
                                {{ \Carbon\Carbon::parse($despesa->data)->format('d/m/Y') }}
                            </td>
                            <td class="col-lg-10">
                                    {{ $despesa->numero }}
                                </td>
                            <td class="col-lg-10">
                                {{ number_format($despesa->valor(), 2, ',', '.') }}
                            </td>                            
                            <td class="col-lg-10">
                                @if ($despesa->carteira)
                                {{ $despesa->carteira->nome }}
                                @endif                                 
                            </td>                            
                            <td class="text-{{ $despesa->aprovada ? 'success' : 'danger' }}">
                                {{ $despesa->aprovada ? 'Aprovada' : 'Não aprovada' }}
                            </td>

                            <td class="text-right">
                                @if(!$despesa->aprovada || Auth::user()->hasRole('admin'))
                                <a href="{{ route('despesas.edit', $despesa->id) }}" class="btn btn-outline-primary btn-rounded"><i class="fa fa-edit"></i></a>
                                @endif

                                @role(['admin', 'manager'])
                                <a href="#" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $despesa->id }}" data-valor="{{ number_format($despesa->valor(), 2, ',', '.') }}" data-nome="{{ $despesa->fornecedor->nome }}"  class="btn btn-outline-danger btn-rounded"><i class="fa fa-trash"></i></a>
                                @endrole
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="clearfix"></p>

            {{ $despesas->links() }}
        </div>
    </div>


    @component('shared._modal')

        @slot('modal', 'modalExcluir')
        @slot('title', 'Excluir empresa')

        <span id="message">Deseja excluir a despesa <strong></strong> de <span id="name"></span>?</span>

        <form method="post" action="{{ route('despesas.delete') }}">
            @csrf
            <input type="hidden" name="id" id="id">
        </form>

        @slot('footer')
            <input type="reset" class="btn btn-primary" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-danger" value="Excluir">
        @endslot


    @endcomponent
@endsection

@push('scripts')
    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var nome = button.data('nome');
            var valor = button.data('valor');
            var modal = $(this);
            modal.find('.modal-body #name').append(" " + nome);
            modal.find('.modal-body #message strong').text(valor);
            modal.find('.modal-body #id').val(id);
        })

        $('#modalExcluir input[type=submit]').on('click', function () {
            $(this).parents().find('form').submit();
        })
    </script>
@endpush
