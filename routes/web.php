<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function (){
    return redirect()->route('dashboard');
});

Route::group(['middleware' => ['auth','web']], function() {
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');
    Route::post('/', 'DashboardController@dashboard')->name('dashboard');



    // -- Receitas
    Route::get('/receitas', 'ReceitasController@list')->name('receitas.list');
    Route::get('/receitas/add', 'ReceitasController@add')->name('receitas.add');
    Route::post('/receitas/store', 'ReceitasController@store')->name('receitas.store');
    Route::get('/receitas/edit/{id}', 'ReceitasController@edit')->name('receitas.edit');
    Route::post('/receitas/update', 'ReceitasController@update')->name('receitas.update');
    Route::post('/receitas/delete', 'ReceitasController@delete')->name('receitas.delete');

    // -- Despesas
    Route::get('/despesas', 'DespesasController@list')->name('despesas.list');
    Route::get('/despesas/add', 'DespesasController@add')->name('despesas.add');
    Route::post('/despesas/store', 'DespesasController@store')->name('despesas.store');
    Route::get('/despesas/edit/{id}', 'DespesasController@edit')->name('despesas.edit');
    Route::post('/despesas/update', 'DespesasController@update')->name('despesas.update');
    Route::post('/despesas/delete', 'DespesasController@delete')->name('despesas.delete');
    Route::post('/despesas/aprovar', 'DespesasController@aprovar')->name('despesas.aprovar');
    Route::post('/despesas/item/add', 'DespesasController@itemAdd')->name('despesas.item.add');
    Route::post('/despesas/item/delete', 'DespesasController@itemDel')->name('despesas.item.delete');

    // -- Parcelas
    Route::get('/contasapagar/', 'ParcelasController@list')->name('contasapagar.list');

    // -- Parcelas das despesas
    Route::get('/despesas/{id}/parcelas', 'ParcelasController@list')->name('despesas.parcelas.list');
    Route::get('/despesas/{id}/parcelas/add', 'ParcelasController@add')->name('despesas.parcelas.add');
    Route::post('/despesas/parcelas/store', 'ParcelasController@store')->name('despesas.parcelas.store');
    Route::get('/despesas/parcelas/edit/{id}', 'ParcelasController@edit')->name('despesas.parcelas.edit');
    Route::post('/despesas/parcelas/update', 'ParcelasController@update')->name('despesas.parcelas.update');
    Route::post('/despesas/parcelas/delete', 'ParcelasController@delete')->name('despesas.parcelas.delete');

    // -- Fornecedores
    Route::get('/fornecedores', 'FornecedoresController@list')->name('fornecedores.list');
    Route::get('/fornecedores/add', 'FornecedoresController@add')->name('fornecedores.add');
    Route::post('/fornecedores/store', 'FornecedoresController@store')->name('fornecedores.store');
    Route::get('/fornecedores/edit/{id}', 'FornecedoresController@edit')->name('fornecedores.edit');
    Route::post('/fornecedores/update', 'FornecedoresController@update')->name('fornecedores.update');
    Route::post('/fornecedores/delete', 'FornecedoresController@delete')->name('fornecedores.delete');

    // -- Carteiras
    Route::get('/carteiras', 'CarteirasController@list')->name('carteiras.list');
    Route::get('/carteiras/add', 'CarteirasController@add')->name('carteiras.add');
    Route::post('/carteiras/store', 'CarteirasController@store')->name('carteiras.store');
    Route::get('/carteiras/edit/{id}', 'CarteirasController@edit')->name('carteiras.edit');
    Route::post('/carteiras/update', 'CarteirasController@update')->name('carteiras.update');
    Route::post('/carteiras/delete', 'CarteirasController@delete')->name('carteiras.delete');

    // -- Centros de custos
    Route::get('/centros', 'CentroCustosController@list')->name('centros.list');
    Route::get('/centros/add', 'CentroCustosController@add')->name('centros.add');
    Route::post('/centros/store', 'CentroCustosController@store')->name('centros.store');
    Route::get('/centros/edit/{id}', 'CentroCustosController@edit')->name('centros.edit');
    Route::post('/centros/update', 'CentroCustosController@update')->name('centros.update');
    Route::post('/centros/delete', 'CentroCustosController@delete')->name('centros.delete');
        // -- Subcategorias
        Route::get('/centros/subcategorias/{id?}', 'CentroCustosController@sub_list')->name('subcategorias.list');
        Route::get('/centros/subcategorias/add/{id?}', 'CentroCustosController@sub_add')->name('subcategorias.add');
        Route::post('/centros/subcategorias/store', 'CentroCustosController@sub_store')->name('subcategorias.store');
        Route::get('/centros/subcategorias/edit/{id}', 'CentroCustosController@sub_edit')->name('subcategorias.edit');
        Route::post('/centros/subcategorias/update', 'CentroCustosController@sub_update')->name('subcategorias.update');
        Route::post('/centros/subcategorias/delete', 'CentroCustosController@sub_delete')->name('subcategorias.delete');

    // -- Items
    Route::get('/items', 'ItemsController@list')->name('items.list');
    Route::get('/items/add', 'ItemsController@add')->name('items.add');
    Route::post('/items/store', 'ItemsController@store')->name('items.store');
    Route::get('/items/edit/{id}', 'ItemsController@edit')->name('items.edit');
    Route::post('/items/update', 'ItemsController@update')->name('items.update');
    Route::post('/items/delete', 'ItemsController@delete')->name('items.delete');


    // - Rotas para configurações

    // -- Empresas
    Route::get('/empresas', 'EmpresasController@list')->name('empresas.list');
    Route::get('/empresas/add', 'EmpresasController@add')->name('empresas.add');
    Route::post('/empresas/store', 'EmpresasController@store')->name('empresas.store');
    Route::get('/empresas/edit/{id}', 'EmpresasController@edit')->name('empresas.edit');
    Route::post('/empresas/update', 'EmpresasController@update')->name('empresas.update');
    Route::post('/empresas/delete', 'EmpresasController@delete')->name('empresas.delete');

    // - Usuarios
    Route::get('/usuarios', 'UsersController@list')->name('users.list');
    Route::get('/usuarios/add', 'UsersController@add')->name('users.add');
    Route::post('/usuarios/store', 'UsersController@store')->name('users.store');
    Route::get('/usuarios/edit/{id}', 'UsersController@edit')->name('users.edit');
    Route::post('/usuarios/update', 'UsersController@update')->name('users.update');
    Route::get('usuarios/password', 'UsersController@password')->name('users.password');
    Route::post('usuarios/password/update', 'UsersController@passwordUpdate')->name('users.password.update');
    Route::post('/usuarios/delete', 'UsersController@delete')->name('users.delete');


    // - Relatorios

    Route::get('/relatorios/contas-a-pagar', 'ReportsController@contasPagar')->name('reports.contas.pagar');

    // - Miscelânea
    Route::get('usuarios/setEmpresa/{id}', 'UsersController@setEmpresa')->name('users.setEmpresa');
    Route::post('misc/setCentro}', 'DashboardController@setCentro')->name('setCentro');
    Route::post('misc/setCarteira}', 'DashboardController@setCarteira')->name('setCentro');
});



// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Auth::routes();
